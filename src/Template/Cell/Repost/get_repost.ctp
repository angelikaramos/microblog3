<?php if ($post != null): ?>
    <?php if ($post->_is_deleted == 1): ?>
        <?= $this->element('url_post', ['repost_post_id' => $post->id]); ?>
    <?php endif; ?>
    <div class="card mt-2">
        <?php if ($post->_is_deleted == 1): ?>
            <div class="card-header">
                <p class="text-muted">This post is unavailable.</p>
            </div>
        <?php endif; ?>
        <?php if ($post->_is_deleted == 0): ?>
            <div class="card-header">
                <!-- Profile picture -->
                <?= $this->Html->image('uploads/' . $post->user->profile_picture, ['alt' => 'Profile Picture', 'height' => '40px']); ?>
                <!-- Poster full name -->
                <span class="font-weight-bold">
                    <?= $this->Html->link($post->user->full_name, [
                        'controller' => 'users',
                        'action' => 'view', $post->user->id], [
                        'class' => 'text-decoration-none text-dark font-weight-bold']
                    ); ?>
                </span>
                <!-- Poster username -->
                <span><?= "@" . h($post->user->username); ?></span>
                <!-- Post date -->
                <span class="card-text">
                    <small class="text-muted">
                        <?= "· " . date('M d', strtotime($post->modified)); ?>
                    </small>
                </span>
            </div>
            <a href="<?= $this->Url->build(['controller' => 'posts', 'action' => 'view', $post->id], ['escape' => false]); ?>" class="text-decoration-none text-dark">
                <div class="card-body">
                    <!-- Post -->
                    <div class="post ml-5 mb-2">
                        <?php if (empty($post->post_image)): ?>
                            <!-- Post text -->
                            <span class="post-content"><?= h($post->post_text); ?></span>
                            <!-- Check if it is also reposted quote, if true get post_id then create url -->
                            <?= $this->cell('Post::getPostId', [$post_repost_id]); ?>
                        <?php endif; ?>
                        <?php if (!empty($post->post_image)): ?>
                            <!-- Post text -->
                            <p class="post-content"><?= h($post->post_text); ?></p>
                            <!-- Post image -->
                            <a href="<?= $this->Url->build(['controller' => 'posts', 'action' => 'view', $post->id], ['escape' => false]); ?>" class="text-decoration-none text-dark">    
                                <?= $this->Html->image('posts/' . h($post->post_image), [
                                    'class' => 'rounded thumb-post-image'
                                ]); ?>
                            </a>
                        <?php endif; ?>
                    </div>
                </div>
            </a>
        <?php endif; ?>
    </div><br>
<?php endif; ?>
