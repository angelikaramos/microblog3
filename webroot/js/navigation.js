// Reload page when back button is pressed.
if(window.performance.getEntriesByType('navigation')[0].type === "back_forward")
{
    console.log('Reloading');
    window.location.reload();
}