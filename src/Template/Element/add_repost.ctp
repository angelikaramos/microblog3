<div class="card ml-3">
    <div class="card-header">
        <?= $this->Html->image('uploads/' . $post_user_profile_picture, ['alt' => 'Profile Picture', 'class' => 'thumb-img-post']); ?>
        <?= $this->Html->link($post_user_full_name, ['controller' => 'users',
        'action' => 'view', $post_id], ['class' => 'text-decoration-none text-dark font-weight-bold']); ?>
        <span><?= "@" . h($post_user_username); ?></span>
    </div>
    <div class="card-body">
        <?php if (empty($post_image)): ?>
            <!-- Post text -->
            <span class="post-content custom-select-lg">
                <?= h($post_text); ?>
            </span>
        <?php endif; ?>
        <?php if (!empty($post_image)): ?>
            <!-- Post text -->
            <p class="post-content">
                <?= h($post_text); ?>
            </p>
            <!-- Post image -->
            <span class="post_image">
                <?= $this->Html->image('posts/' . h($post_image),[
                    'class' => 'thumb-post-image'
                ]); ?>
            </span>
        <?php endif; ?>
        <!-- Reposted post -->
        <?php if (!empty($repost)): ?>
            <?php if ($repost->is_deleted == 1): ?>
                <?= $this->element('url_post', ['repost_post_id' => $repost->id]); ?>
                <div class="card-header mt-2">
                    <p class="text-muted">This post is unavailable.</p>
                </div>
            <?php endif; ?>
            <?php if ($repost->is_deleted == 0): ?>
                <!-- Check if it is also reposted quote -->
                <?= $this->Url->build(['controller' => 'posts', 'action' => 'view', $repost->id]) ?>
            <?php endif; ?>
        <?php endif; ?>
        <p class="card-text mt-2">
            <small class="text-muted">
                <?= date('g:i A - j M Y', strtotime($post_modified)); ?>
            </small>
        </p>
    </div>
</div>