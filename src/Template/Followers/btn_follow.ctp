<?= $this->Html->link('Follow', ['controller' => 'followers', 'action' => 'add', $user_id], 
    ['data-id' => $user_id, 'class' => 'add-follow btn btn-primary btn-sm float-right mr-2 mb-2']
); ?>