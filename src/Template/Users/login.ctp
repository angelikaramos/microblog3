<div class="users form">
    <div class="login-container d-flex align-items-center justify-content-center">
        <?= $this->Form->create($user, [
            'autocomplete' => 'off',
        ]); ?>
            <h4 class="mb-5 text-center">LOGIN</h4>
            <fieldset>
                <div class="error-message text-center">
                    <?=  $this->Form->error('username', null, ['wrap' => 'label', 'class' => 'text-danger']); ?>
                </div>
                <?= $this->Form->control('username', [
                    'label' => false,
                    'class' => 'form-control rounded-pill form-control-lg mb-3',
                    'placeholder' => 'Username',
                    'required' => false,
                    'error' => false
                ]); ?>
                <div class="error-message text-center">
                    <?=  $this->Form->error('password', null, ['wrap' => 'label', 'class' => 'text-danger']); ?>
                </div>
                <?= $this->Form->control('password', [
                    'label' => false,
                    'class' => 'form-control rounded-pill form-control-lg mb-3',
                    'placeholder' => 'Password',
                    'required' => false,
                    'error' => false
                ]); ?>
            </fieldset>
            <p class="mt-3 font-weight-normal">
                Don't have account yet?
                <?= $this->Html->link('Click here', [
                    'controller' => 'users', 
                    'action' => 'add'
                ]); ?>
                to register.
            </p>     
            <?= $this->Form->button(__('Login'), [
                'type' => 'submit', 
                'class' => 'btn btn-primary text-uppercase btn-block rounded-pill btn-lg'
            ]); ?>
        <?= $this->Form->end() ?>
    </div>
</div>