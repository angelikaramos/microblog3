<div class="post mt-3">
    <?php 
        // Add dropdown_post element if logged in user is the one who post
        if ($loggedin_user_id == $repost_user_id) {
            echo $this->element('dropdown_post', ['post_id' => $repost_id]); 
        }
        if ($loggedin_user_id == $post_user_id) {
            // Repost label
            echo "<span class='small text-primary'>";
            echo "<i class='fa fa-retweet mr-1' aria-hidden='true'></i>";
            echo "You reposted";
            echo "</span>";
        }
        if ($loggedin_user_id != $post_user_id) {
            // Repost label
            echo "<span class='small text-primary'>";
            echo "<i class='fa fa-retweet mr-1' aria-hidden='true'></i>";
            echo "Reposted by " . $post_username;
            echo "</span>";
        }
    ?>
    <!-- Post user -->
    <div class="post-user mt-2">
        <?= $this->Html->image('uploads/' . $repost_user_profile_picture, 
        ['alt' => 'Profile Picture', 'class' => 'thumb-img-post']); ?>
        <span class="font-weight-bold">
            <?= $this->Html->link(
                $repost_user_full_name, [
                'controller' => 'users',
                'action' => 'view', $repost_user_id], [
                'class' => 'text-decoration-none text-dark font-weight-bold']
            ); ?>
        </span>
        <span><?= "@" . h($repost_user_username); ?></span>
    </div>
    <!-- Post -->
    <div class="post ml-5">
        <?php if (empty($repost_post_image)): ?>
            <span class="post-content">
                <!-- Post text -->
                <?= h($repost_post_text); ?>
                <!-- Repost Post -->
                <?= $this->cell('Repost::getRepost', [$repost_id, $repost_repost_id]); ?> 
            </span>
        <?php endif; ?>
        <?php if (!empty($repost_post_image)): ?>
            <!-- Post text -->
            <p class="post-content"><?= h($repost_post_text); ?></p>
            <!-- Post image -->
            <a href="<?= $this->Url->build(['controller' => 'posts', 'action' => 'view', $repost_id], ['escape' => false]); ?>" class="text-decoration-none text-dark">  
                <?= $this->Html->image('posts/' . h($repost_post_image), [
                    'class' => 'rounded thumb-post-image'
                ]); ?>
            </a>
        <?php endif; ?>
    </div>
    <!-- Post date -->
    <small class="text-muted">
        <?= date('g:i A · M, j Y', strtotime($repost_modified)); ?>
    </small><hr>
    <!-- Buttons -->
    <?= $this->element('view_buttons_repost', [
        'post_id' => $post_id,
        'repost_id' => $repost_id,
        'repost_id' => $repost_id
    ]); ?>
</div><hr>