<?php

namespace App\Controller;

use Cake\Event\Event;
use Cake\Http\Exception\NotFoundException;

class FollowersController extends AppController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Component
        $this->loadComponent('Paginator');
        $this->loadComponent('GetUser');
        $this->loadComponent('RequestHandler');

        // Load Model
        $this->loadModel('Users');

        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        // Get logged in user full_name for side_navbar
        if ($this->Auth->user()) {
            $loggedin_user = $this->Users->find('all', [
                'conditions' => ['id' => $user_id],
                'fields' => ['full_name'],
            ])->first();
        }
        $this->set('full_name', $loggedin_user->full_name);
    }

    /**
     * Add follower
     * @param $user_id
     * @return redirected page
     */
    public function add(int $user_id)
    {
        // Get logged in user_id
        $follower_user_id = $this->Auth->user('id');

        // Check if follower data is existing
        $checkFollower = $this->Followers->findAllByUser_idAndFollower_user_id($user_id, $follower_user_id);

        // Update column deleted, _is_deleted if data already exists
        if (!$checkFollower->isEmpty()) {
            // Get follower data
            $follower = $this->Followers->find('all')->where(['user_id' => $user_id, 'follower_user_id' => $follower_user_id])->contain(['Users'])->first();

            // Set and save
            $follower->deleted = null;
            $follower->_is_deleted = 0;
            if ($this->Followers->save($follower)) {
                // Handle ajax
                if ($this->request->is('ajax')) {
                    $this->set('user_id', $user_id);
                    $this->render('btn_followed', 'ajax');
                } else {
                    return $this->redirect($this->referer());
                }
            } 
        }

        // Add follower data if data is not yet exist
        if ($checkFollower->isEmpty()) {
            $follower = $this->Followers->newEntity();

            // Set and save
            $follower->user_id = $user_id;
            $follower->follower_user_id = $follower_user_id;
            $follower->_is_deleted = 0;
            if ($this->Followers->save($follower)) {
                // Handle ajax
                if ($this->request->is('ajax')) {
                    $this->set('user_id', $user_id);
                    $this->render('btn_followed', 'ajax');
                } else {
                    return $this->redirect($this->referer());
                }
            }
        }
    }

    /**
     * Display logged in user following
     * 
     * @return object data
     */
    public function following()
    {
        // Set layout
        $this->viewBuilder()->setLayout('logged_in');

        // Get logged in user_id
        $follower_user_id = $this->Auth->user('id');

        // Get users not yet followed for side_navbar
        $usersNotfollowed = $this->GetUser->getUsersNotYetFollowed($follower_user_id);

        // Get all logged in user following
        $settings = [
            'conditions' => [
                'AND' => ['follower_user_id' => $follower_user_id, '_is_deleted' => 0] 
            ],
            'limit' => '4',
            'order' => ['Followers.modified' => 'desc'],
            'contain' => ['FollowingUser']
        ];
        
        // When unfollowing users then go to page 2, catch NotFoundException then redirect to page 1
        try {
            $usersfollowing = $this->Paginator->paginate($this->Followers, $settings);
        } catch (NotFoundException $e) {
            // Get current page
            $page = $this->request->getQuery('page');
            if ( $page > 1 ) {
                $page -= 1;
                // Redirect to page 1
                return $this->redirect(['controller' => 'followers', 'action' => 'following', '?' => ['page' => $page]]);
            }
        }

        $this->set('usersNotFollowed', $usersNotfollowed);
        $this->set('usersfollowing', $usersfollowing);
    }

    /**
     * Delete follower
     * @param $id
     * @return redirected page
     */
    public function delete(int $user_id)
    {
        // Get logged in user_id
        $follower_user_id = $this->Auth->user('id');

        $follower = $this->Followers->find('all', [
            'conditions' => ['user_id' => $user_id, 'follower_user_id' => $follower_user_id]
        ])->first();

        // Set and save for - soft delete
        $follower->deleted = date('Y-m-d H:i:s');
        $follower->_is_deleted = 1;
        if ($this->Followers->save($follower)) {
            // Handle ajax
            if ($this->request->is('ajax')) {
                $this->set('user_id', $user_id);
                $this->render('btn_follow', 'ajax');
            } else {
                return $this->redirect($this->referer());
            }
        }
    }

    /**
     * Display logged in user followers
     * 
     * @return object data
     */
    public function follower()
    {
        // Set layout
        $this->viewBuilder()->setLayout('logged_in');

        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        // Load Component
        $this->loadComponent('GetUser');

        // Get users not yet followed for side_navbar
        $usersNotfollowed = $this->GetUser->getUsersNotYetFollowed($user_id);

        $settings = [
            'conditions' => [
                'AND' => [
                    'user_id' => $user_id,
                    '_is_deleted' => 0
                ]
            ],
            'limit' => '4',
            'order' => ['Followers.modified' => 'desc'],
            'contain' => ['FollowerUser']
        ];

        // Get logged in user following
        $usersfollowing = $this->GetUser->getUsersAlreadyFollowed($user_id);
        $usersfollower = $this->Paginator->paginate($this->Followers, $settings);

        $this->set('usersNotFollowed', $usersNotfollowed);
        $this->set('usersfollowing', $usersfollowing);
        $this->set('usersfollower', $usersfollower);
    }

    public function follow()
    {
        // Set layout
        $this->viewBuilder()->setLayout('logged_in');

        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        // Load Model
        $this->loadModel('Users');

        // Get the list of user_id that user logged in follows
        $conditions = ['AND' => ['follower_user_id' => $user_id, '_is_deleted' => 0]];
        $usersFollowed = $this->Followers->find('list', [
            'conditions' => $conditions,
            'fields' => ['user_id']
        ]);

        // Get users where id is not in $usersFollowed and id 
        $settings = [
            'conditions' => [
                'id NOT IN' => $usersFollowed,
                '_is_email_activated !=' => '0',
                'id !=' => $user_id],
            'limit' => '4', 
            
        ];

        // When following then go to page 2, catch NotFoundException then redirect to page 1
        try {
            // Get logged in user following
            $usersfollowing = $this->GetUser->getUsersAlreadyFollowed($user_id);
            $follows = $this->Paginator->paginate($this->Users, $settings);
        } catch (NotFoundException $e) {
            // Get current page
            $page = $this->request->getQuery('page');
            if ( $page > 1 ) {
                $page -= 1;
                // Redirect to page 1
                return $this->redirect(['controller' => 'followers', 'action' => 'follow', '?' => ['page' => $page]]);
            }
        }
        
        $this->set('usersfollowing', $usersfollowing);
        $this->set('follows', $follows);
    }
}