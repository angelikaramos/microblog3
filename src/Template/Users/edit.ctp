<main>
    <div class="container-fluid">
        <h2 class="mt-4 mb-4">Edit Profile</h2>
        <?= $this->Form->create($user,  [
            'type' => 'file', 
            'autocomplete' => 'off',
            'novalidate' => true
        ]); ?>
            <?= $this->Form->control('full_name', [
                    'class' => 'form-control mb-2',
                    'required' => false,
                    'error' => false
            ]); ?>
            <div class="error-message">
                <?= $this->Form->error('full_name', null, ['wrap' => 'label']); ?>
            </div>
            <?= $this->Form->control('username', [
                    'class' => 'form-control mb-2',
                    'required' => false,
                    'error' => false
            ]); ?>
            <div class="error-message">
                <?= $this->Form->error('username', null, ['wrap' => 'label']); ?>
            </div>
            <?= $this->Form->control('email', [
                    'class' => 'form-control mb-2',
                    'required' => false,
                    'error' => false
            ]); ?>
            <div class="error-message">
                <?= $this->Form->error('email', null, ['wrap' => 'label']); ?>
            </div>
            <?= $this->Form->control('password', [
                    'class' => 'form-control mb-2',
                    'placeholder' => 'Password',
                    'type' => 'password',
                    'required' => false,
                    'error' => false
            ]); ?>
            <div class="error-message">
                <?= $this->Form->error('password', null, ['wrap' => 'label']); ?>
            </div>
            <?= $this->Form->control('profile_picture', ['type'=>'file',
                    'class' => 'form-control mb-2',
                    'error' => false
            ]); ?>
            <div class="error-message">
                <?= $this->Form->error('profile_picture', null, ['wrap' => 'label']); ?>
            </div>
            <?= $this->Form->button(__('Submit'), [
                'type' => 'submit', 
                'class' => 'btn btn-primary float-right mt-2',
                'id' => 'edit_profile'
            ]); ?>
        <?php echo $this->Form->end(); ?>
    </div>
</main>
