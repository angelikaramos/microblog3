<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class FollowersTable extends Table
{
    public function initialize(array $config)
    {
        // Association
        $this->belongsTo('FollowingUser', [
            'className' => 'Users',
            'foreignKey' => 'user_id',
        ]);
        $this->belongsTo('FollowerUser', [
            'className' => 'Users',
            'foreignKey' => 'follower_user_id',
        ]);
        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'user_id',
        ]);
        
        // Enable timestamp behavior
        $this->addBehavior('Timestamp');
    }
}