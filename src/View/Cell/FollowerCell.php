<?php

namespace App\View\Cell;

use Cake\View\Cell;

class FollowerCell extends Cell
{
    /**
     * Count followers
     * @param $user_id
     * @return int
     */
    public function countFollower($user_id)
    {
        $this->loadModel('Followers');
        $followers = $this->Followers->find('all', [
            'conditions' => ['user_id' => $user_id, '_is_deleted' => 0]
        ]);

        $this->set('countFollower', $followers->count());
    }

    /**
     * Count following
     * @param $user_id
     * @return int
     */
    public function countFollowing($user_id)
    {
        $this->loadModel('Followers');
        $following = $this->Followers->find('all', [
            'conditions' => ['follower_user_id' => $user_id, '_is_deleted' => 0]
        ]);

        $this->set('countFollowing', $following->count());
    }
}