<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;

class PostsTable extends Table
{
    public function initialize(array $config)
    {
        // Association
        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'user_id'
        ]);
        $this->belongsTo('Reposts', [
            'className' => 'Reposts',
            'foreignKey' => 'repost_id'
        ]);
        $this->hasMany('Reposts');
        $this->hasMany('Likes');

        // Enable timestamp behavior
        $this->addBehavior('Timestamp');
    }

    // Validations for length of post_text and allowing empty string in repost
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('post_text', [
            'length' => [
                'rule' => ['maxlength', 140]
                ]
            ])
            ->allowEmptyString('post_text');

        return $validator;
    }

    // Validation for post_image
    public function validationUpload(Validator $validator)
    {
        $validator
            ->allowEmpty('post_image')
            ->add('post_image', [
                'validExtension' => [
                    'rule' => ['extension', ['jpeg', 'png', 'jpg']],
                ]
            ]);
        
        return $validator;
    }
}