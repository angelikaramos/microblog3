<div class="btn-group-sm float-right">
    <!-- Like -->
    <div class="d-inline p-2" id="btnLike<?= $repost_post_id ?>">
        <?= $this->cell('Buttons::checkLike', [$repost_post_id, $loggedin_user_id]); ?>
        <?= $this->cell('Buttons::countLike', [$repost_post_id]); ?>
    </div>
    <!-- Comment -->
    <div class="d-inline p-2">
        <?= $this->Html->link('<span class="far fa-comment"></span><span class="sr-only">', ['controller' => 'posts', 'action' => 'view',  $post_id], ['escape' => false, 'class' => 'text-decoration-none text-dark']); ?>
        <?= $this->cell('Buttons::countComment', [$repost_post_id]); ?>
    </div>
    <!-- Repost -->
    <div class="d-inline p-2">
        <!-- If reposted post -->
        <?php if (!empty($repost_id)): ?>
            <!-- Repost without quote -->
            <?php if (empty($post_text)): ?>
                <?= $this->cell('Buttons::checkRepostByPostId', [
                    $repost_post_id, 
                    $loggedin_user_id]
                ); ?>
                <?= $this->cell('Buttons::countRepost', [$repost_post_id]); ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>