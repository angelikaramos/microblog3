<!-- Dropdown Post -->
<div class="dropdown">
    <i class="fas fa-chevron-down float-right" type="button" id="dropdownMenuButton" 
    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <?= $this->Html->link('Edit Post', [
            'controller'=>'posts','action'=>'edit',  $post_id],
            ['class' => 'dropdown-item']
        ); ?>
        <?= $this->Form->postLink('Delete Post', [
            'controller'=>'posts','action'=>'delete',  $post_id],
            ['class' => 'dropdown-item', 'confirm' => 'Are you sure want to delete this post?']
        ); ?>
    </div>
</div>