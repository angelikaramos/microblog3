<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\ORM\TableRegistry;

class UsersController extends AppController
{
    /**
     * Executed before every action
     * 
     * @return redirected page
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        // Allow users to register and activate account.
        $this->Auth->allow('add', 'activate');

        // Component
        $this->loadComponent('Paginator');
    }

    /**
     * View User
     * @param $id
     * @return object data
     */
    public function view($user_id = null)
    {
        // Set layout
        $this->viewBuilder()->setLayout('logged_in');

        // Component
        $this->loadComponent('Paginator');
        $this->loadComponent('GetUser');

        // Get logged in user_id
        $loggedin_user_id = $this->Auth->user('id');
        $this->set('loggedin_user_id', $loggedin_user_id);

        // Get logged in user full_name for side_navbar
        if ($this->Auth->user()) {
            $loggedin_user = $this->Users->find('all', [
                'conditions' => ['id' => $loggedin_user_id],
                'fields' => ['full_name'],
            ])->first();
        }

        // Get users not yet followed for side_navbar
        $usersNotfollowed = $this->GetUser->getUsersNotYetFollowed($loggedin_user_id);
        $this->set('usersNotFollowed', $usersNotfollowed);

        if (empty($user_id)) {
            // Get logged in user_id
            $user_id = $this->Auth->user('id');
        }

        // Get user data for profile or viewing user info
        $user = $this->Users->get($user_id);

        // Load Model
        $this->loadModel('Posts');
        $this->loadModel('Reposts');
        $this->loadModel('Followers');

        // Get post data
        $posts = $this->Posts->find('all', [
            'conditions' => ['user_id' => $user_id, '_is_deleted' => 0],
            'contain' => ['Users'],
            'order' => ['Posts.modified' => 'desc']
        ]);

        // Get repost data
        $reposts = $this->Reposts->find('all')->contain(['Posts' => ['Users'], 'Users']);

        $settings = [
            'conditions' => ['user_id' => $user_id, '_is_deleted' => 0],
            'contain' => ['Users'],
            'order' => ['Posts.modified' => 'desc'],
            'limit' => '2',
        ];

        $posts = $this->Paginator->paginate($posts, $settings);

        $this->set('full_name', $loggedin_user->full_name);
        $this->set('posts', $posts);
        $this->set('reposts', $reposts);
        $this->set('user_id', $user_id);
        $this->set(compact('user'));
    }

    /**
     * Add user
     * 
     * @return redirected page
     */
    public function add()
    {
        // Set layout
        $this->viewBuilder()->setLayout('not_logged_in');

        // Will not redirect to register page if already logged in
        if ($this->Auth->user()) {
            return $this->redirect($this->Auth->redirectUrl());
        }

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            // Get data from form
            $user = $this->Users->patchEntity($user, $this->request->getData(), ['validated' => 'default']);

            // Get data and use for email verification
            $user_full_name = $user->full_name;
            $user_email = $user->email;

            // Set code_of_activation, _is_email_activated and profile_picture before saving to database
            $bytes = random_bytes(10);
            $code_of_activation = bin2hex($bytes);
            $user->code_of_activation = $code_of_activation;
            $user->_is_email_activated = 0;
            $user->profile_picture = 'profile_picture.png';

            // For email verification
            $email = new Email('default');
            if ($this->Users->save($user)) {
                // Get template from Template/Email/email_verification.ctp
                $email->setFrom(['microblogfamily@gmail.com' => 'MICROBLOG'])
                    ->setTo($user_email)
                    ->setSubject('Email Verification')
                    ->setEmailFormat('html')
                    ->setViewVars([
                        'name' => $user_full_name, 
                        'code_of_activation' => $code_of_activation
                    ])
                    ->viewBuilder()
                        ->setTemplate('email_verification');
                if ($email->send()) {
                    // Flash message
                    $this->Flash->success(__(
                        'Registration success. A link has been sent to your email.'
                    ));
                } else {
                    // Flash message
                    $this->Flash->error(__(
                        'Failed in sending link to your email. Please try again.'
                    ));
                }
                return $this->redirect(['action' => 'add']);
            }
            $this->Flash->error(__('Registration failed. Please try again.'));
        }
        $this->set('user', $user);
    }

    /**
     * Login account
     * 
     * @return redirected page
     */
    public function login()
    {
        // Set layout
        $this->viewBuilder()->setLayout('not_logged_in');
        
        // Will not redirect to register page if already logged in
        if ($this->Auth->user()) {
            return $this->redirect($this->Auth->redirectUrl());
        }

        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            // Validate username and password using login validation
            $user = $this->Users->newEntity($this->request->getData(), ['validate' => 'login']);
            if ($user->getErrors()) {
                // Pass context to view
                $this->set('user', $user);
                // Flash Message
                return $this->Flash->error(__(
                    'Login failed. Please try again.')
                );
            }

            // Get user data from form
            $userdata = $this->Users->patchEntity($user, $this->request->getData(), ['validate' => 'login']);
            $username = $userdata->username;

            // Get other user data and check if email is activated or not
            $getUser = $this->Users->find('all', [
                'conditions' => [
                    'username' => $username
                ]
            ])->first();
            
            // If user data exist
            if (!empty($getUser)) {
                // Get _is_email_activated value from $getUser
                $getUserIsEmailActivated = $getUser->_is_email_activated;

                // If email is not activated
                if ($getUserIsEmailActivated == 0) {
                    // Pass context to view
                    $this->set('user', $user);
                    // Flash message
                    return $this->Flash->error(__('Please activate your account. 
                    A link has been sent to your email.'));
                }

                // If email is activated
                if ($getUserIsEmailActivated == 1) {
                    // Returns true if login was successful
                    $user = $this->Auth->identify();
                    if ($user) {
                        $this->Auth->setUser($user);
                        return $this->redirect($this->Auth->redirectUrl());
                    }
                }
            }
            // Pass context to view
            $user = $this->Users->newEntity();
            $this->set('user', $user);
            // Flash message
            $this->Flash->error(__('Invalid credentials. Please try again.'));
        }
        // Pass context to view
        $this->set('user', $user);
    }

    /**
     * Logout account
     * 
     * @return redirected page
     */
    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    /**
     * Activate account
     * @param $code_of_activation
     * @return redirected page
     */
    public function activate($code_of_activation)
    {
        if (!empty($code_of_activation)) {
            // Get user data where code_of_activation is equal to param and _is_email_activated == 0
            $getUser = $this->Users->find('all', [
                'conditions' => [
                    'code_of_activation' => $code_of_activation,
                    '_is_email_activated' => 0
                ]
            ])->first();

            if (!empty($getUser)) {
                // Get user_id from $getUser
                $user_id = $getUser->id;

                // Update is_email_activated and code_of_activation
                $user = $this->Users->get($user_id);
                $user->code_of_activation = null;
                $user->_is_email_activated = 1;

                if ($this->Users->save($user)) {
                    // Flash message
                    $this->Flash->success(__('Your account has been activated successfully. Please login.'));
                    return $this->redirect(['controller' => 'users', 'action' => 'login']);
                } else {
                    // Flash message
                    $this->Flash->error(__('Failed in activating your account. Please try again'));
                    return $this->redirect(['controller' => 'users', 'action' => 'login']);
                }
            }
        }
    }

    /**
     * Edit user
     * 
     * @return redirected page
     */
    public function edit()
    {
        // Set layout
        $this->viewBuilder()->setLayout('logged_in_form');

        // Component
        $this->loadComponent('GetUser');

        // Get logged in user_id
        $user_id = $this->Auth->user('id');
        $this->set('loggedin_user_id', $user_id);

        // Get logged in user full_name for side_navbar
        if ($this->Auth->user()) {
            $loggedin_user = $this->Users->find('all', [
                'conditions' => ['id' => $user_id],
                'fields' => ['full_name'],
            ])->first();
        }
        $this->set('full_name', $loggedin_user->full_name);

        // Get users not yet followed for side_navbar
        $usersNotfollowed = $this->GetUser->getUsersNotYetFollowed($user_id);
        $this->set('usersNotFollowed', $usersNotfollowed);

        $user = $this->Users->get($user_id);
        if ($this->request->is(['post', 'put'])) {
            $data = $this->request->getData();

            // Check if uploaded profile picture is more than 10mb
            if ($data['profile_picture']['size'] >= 10000000) {
                // Flash message
                $this->Flash->error(__(
                    'Unable to update your profile. Image must be less than 10MB.'
                ));
                return $this->redirect($this->referer());
            }

            // If empty password unset password
            if (empty($data['password'])) {
                unset($data['password']);
            }

            // If empty profile_picture set old profile_picture
            if (empty($data['profile_picture']['name'])) {
                $data['profile_picture']['name'] = $user->profile_picture;
            }

            // If profile_picture is not empty upload image and set new profile_picture
            if (!empty($data['profile_picture']['name'])) {
                $tmp = $data['profile_picture']['tmp_name']; 
                $image = $data['profile_picture']['name'];
                $target = WWW_ROOT.'img'.DS.'uploads'.DS; 
                $target = $target.basename($image); 
                move_uploaded_file($tmp, $target);
                $data['profile_picture'] = $data['profile_picture']['name'];
            }

            // If old email not equal to new email set fields
            if ($user->email !== $data['email']) {
                // Set code_of_activation value before saving to database
                $bytes = random_bytes(10);
                $code_of_activation = bin2hex($bytes);
                $user->code_of_activation = $code_of_activation;
                $user->_is_email_activated = 0;

                $user_name = $user->full_name;
                $user_email = $data['email'];
                
                $email_change = 1;
            }
            $this->Users->patchEntity($user, $data);
            if ($this->Users->save($user)) { 

                if (!empty($email_change)) {
                    // Send link for email verification
                    $email = new Email('default');

                    // Get template from Template/Email/email_updated.ctp
                    $email->setFrom(['microblogfamily@gmail.com' => 'MICROBLOG'])
                    ->setTo($user_email)
                    ->setSubject('Email Verification')
                    ->setEmailFormat('html')
                    ->setViewVars([
                        'name' => $user_name, 
                        'code_of_activation' => $code_of_activation
                    ])
                    ->viewBuilder()
                        ->setTemplate('email_updated');
                    if ($email->send()) {
                        // Flash message
                        $this->Flash->success(__('Your profile has been updated.
                        A link has been sent to your new email to activate your account.'));
                        return $this->redirect(array('action' => 'logout'));
                    }
                }
                if (!empty($data['password'])) {
                    // Flash message
                    $this->Flash->success(__('Your profile has been updated.
                    Please login and use your new password.'));
                    return $this->redirect(array('action' => 'logout'));
                }
                // Flash message
                $this->Flash->success(__('Your profile has been updated.'));
                return $this->redirect(['action' => 'view', $user_id]);
            }
            // Flash message
            $this->Flash->error(__('Unable to update your profile.'));
        }
        else {
            // Unset password value in form
            $user = $this->Users->get($user_id);
            unset($user['password']);
        }
        $this->set('user', $user);
    }

    /**
     * Search user
     * 
     * @return object data
     */
    public function search()
    {
        // Set layout
        $this->viewBuilder()->setLayout('logged_in');

        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        // Get logged in user full_name for side_navbar
        if ($this->Auth->user()) {
            $loggedin_user = $this->Users->find('all', [
                'conditions' => ['id' => $user_id],
                'fields' => ['full_name'],
            ])->first();
        }

        // Component
        $this->loadComponent('Paginator');
        $this->loadComponent('GetUser');

        // Access session
        $session = $this->getRequest()->getSession();

        // Get logged in user_id
        $loggedin_user_id = $this->Auth->user('id');
        $this->set('loggedin_user_id', $loggedin_user_id);

        // Get users not yet followed for side_navbar
        $usersNotfollowed = $this->GetUser->getUsersNotYetFollowed($loggedin_user_id);
        $this->set('usersNotFollowed', $usersNotfollowed);

        if (empty($user_id)) {
            // Get logged in user_id
            $user_id = $this->Auth->user('id');
        }
        
        $user = $this->request->getData();

        // Get search keyword from search bar form
        if (!empty($user['keyword'])) {
            $keyword = $user['keyword'];

            // Sanitize keyword input
            $keyword = strip_tags(addslashes($keyword));
            $keyword = htmlspecialchars($keyword);

            $condition = [];
            $condition['Users.full_name LIKE'] = "%" . trim($keyword) . "%";
            $condition['Users.username LIKE'] = "%" . trim($keyword) . "%";
            $conditionsLogic['OR'] = $condition;

            $session->write('Config.conditionsLogic', $conditionsLogic);
            $session->write('Config.keyword', $keyword);
        }

        // When Next page clicked get session conditionsLogic and keyword
        if ($session->check('Config.conditionsLogic')) {
            $conditionsLogic = $session->read('Config.conditionsLogic');
            $user['keyword'] = $session->read('Config.keyword');
        }

        $settings = [
            'conditions' => [
                'OR' => ['id !=' => $user_id], $conditionsLogic
            ],
            'limit' => 4
        ];

        $users = $this->Paginator->paginate($this->Users, $settings);
        $usersfollowing = $this->GetUser->getUsersAlreadyFollowed($user_id);
        $this->set('usersfollowing', $usersfollowing);
        $this->set('full_name', $loggedin_user->full_name);
        $this->set('keyword', $user['keyword']);
        $this->set('users', $users);
    }
}