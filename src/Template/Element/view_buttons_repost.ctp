<div class="btn-group-sm align-center">
    <!-- Like -->
    <div class="d-inline p-2" id="btnLike<?= $post_id ?>">
        <?= $this->cell('Buttons::checkLike', [$repost_id, $loggedin_user_id]);  ?>
        <?= $this->cell('Buttons::countLike', [$repost_id]);  ?>
    </div>
    <!-- Comment -->
    <div class="d-inline p-2">
        <?= $this->Html->link('<span class="far fa-comment"></span><span class="sr-only">', [
            'controller' => 'posts', 'action' => 'view',  $repost_id], ['escape' => false, 'class' => 'text-decoration-none text-dark']);  ?>
        <?= $this->cell('Buttons::countComment', [$repost_id]);  ?>
    </div>
    <!-- Repost -->
    <div class="d-inline p-2">
        <?= $this->cell('Buttons::checkRepostByPostId', [$repost_id, $loggedin_user_id]);  ?>
        <?= $this->cell('Buttons::countRepost', [$repost_id]);  ?>
    </div>
</div>