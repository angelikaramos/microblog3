<!-- Dropdown Comment -->
<div class="dropdown">
    <i class="fas fa-chevron-down float-right" type="button" id="dropdownMenuButton" 
    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <?= $this->Html->link('Edit Comment', [
            'controller'=>'comments',
            'action'=>'edit',  $comment_id], 
            ['class' => 'dropdown-item']
        ); ?>
        <?= $this->Form->postLink('Delete Comment', 
            ['controller'=>'comments',
            'action'=>'delete',  $comment_id], 
            ['class' => 'dropdown-item', 'confirm' => 'Are you sure want to delete this comment?']
        ); ?>
    </div>
</div>