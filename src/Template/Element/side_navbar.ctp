<nav class="sb-sidenav accordion sb-sidenav-light" id="sidenavAccordion">
    <div class="sb-sidenav-menu">
        <div class="nav">
            <a href="<?= $this->Url->build(['controller' => 'posts', 'action' => 'index'], 
                ['escape' => false]); ?>" class="nav-link">
                <div class="sb-nav-link-icon"></div>
                <strong>HOME</strong>
            </a>
            <a href="<?= $this->Url->build(['controller' => 'users', 'action' => 'view'], 
                ['escape' => false]); ?>" class="nav-link">
                <div class="sb-nav-link-icon"></div>
                <strong>PROFILE</strong>
            </a>
            <a href="<?= $this->Url->build(['controller' => 'followers', 'action' => 'following'], 
                ['escape' => false]); ?>" class="nav-link">
                <div class="sb-nav-link-icon"></div>
                <strong>FOLLOWING</strong>
            </a>
            <a href="<?= $this->Url->build(['controller' => 'followers', 'action' => 'follower'], 
                ['escape' => false]); ?>" class="nav-link">
                <div class="sb-nav-link-icon"></div>
                <strong>FOLLOWERS</strong>
            </a>
            <br>
            <!-- Who to follow Section -->
            <?php if (isset($usersNotFollowed)): ?>
                <div class="sb-nav ml-3">
                    <strong>Who to follow:</strong><br><br>
                    <!-- If $usersNotFollowed is empty -->
                    <?php 
                        if ($usersNotFollowed->isEmpty()) {
                            echo "<p> Users you may be interested in will show here. </p>";
                        }
                    ?>
                    <?php foreach ($usersNotFollowed as $userNotFollowed): ?>
                        <div id="follow-list">
                            <?= $this->Html->link($userNotFollowed->full_name, ['controller' => 'users', 'action' => 'view', $userNotFollowed->id], ['class' => 'text-decoration-none text-dark font-weight-bold']
                            ); ?><br>
                            <span class="mr-3 small"><?= "@" . $userNotFollowed->username; ?></span>
                            <span class="follow<?= $userNotFollowed->id ?>">
                                <?= $this->Html->link('Follow', ['controller' => 'followers', 
                                'action' => 'add',  $userNotFollowed->id],
                                ['data-id' => $userNotFollowed->id, 'class' => 'add-follow btn btn-primary btn-sm float-right mr-2 mb-2']) ?>
                            </span><br><br>
                        </div> 
                    <?php endforeach; ?> 
                </div>
                <?php 
                    if (!$usersNotFollowed->isEmpty()) {
                        echo $this->Html->link('Show more', ['controller' => 'followers', 
                        'action' => 'follow'], ['class' => 'btn btn-light btn-sm float-right text-primary']);
                    }
                ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="sb-sidenav-footer">
        <div class="small">Logged in as:</div>
        <h5><?= mb_strimwidth(h($full_name), 0, 20, "...") ?></h5>
    </div>
</nav>