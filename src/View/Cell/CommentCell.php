<?php

namespace App\View\Cell;

use Cake\View\Cell;

class CommentCell extends Cell
{
    /**
     * Get comments in certain post
     * @param $post_id
     * @return object data
     */
    public function getComments($post_id, $loggedin_user_id)
    {
        // Load Model
        $this->loadModel('Comments');

        // Get comment data by post_id
        $comments = $this->Comments->find('all', [
            'conditions' => ['post_id' => $post_id, '_is_deleted' => 0],
            'contain' => ['Users']
        ]);

        $this->set('loggedin_user_id', $loggedin_user_id);
        $this->set('comments', $comments);
    }
}