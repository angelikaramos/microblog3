<nav class="sb-topnav navbar navbar-expand navbar-dark bg-light">
    <!-- Title -->
    <h4 class="ml-4">MICROBLOG</h4>
    <!-- Side Bar Toggle Button -->
    <button class="btn btn-link btn-sm order-1 order-lg-0 bg-d text-dark" id="sidebarToggle" href="#"> 
        <i class="fas fa-bars"></i>
    </button>
    <!-- Search bar -->
    <?php
        if (isset($keyword)) {
            $keyword =  $keyword;
        }
        if (empty($keyword)) {
            $keyword = "";
        }
    ?>
    <?= $this->Form->create(null, [
        'url' => ['controller' => 'users', 'action' => 'search'], 
        'class' => 'd-md-inline-block form-inline mx-auto', 
        'autocomplete' => 'off'
    ]); ?>
    <div class="input-group">
        <?= $this->Form->control('keyword', [
            'class' => 'form-control',
            'placeholder' => 'Search for...',
            'label' => false,
            'id' => 'keyword',
            'value' => $keyword
        ]); ?>
        <div class="input-group-append">
            <?= $this->Form->button(__('Search'), [
                'class' => 'btn btn-primary',
                'id' => 'search'
            ]); ?>
        </div>
        <?= $this->Form->end(); ?>
    </div>
    <!-- Logout-->
    <div class="navbar-nav ml-auto ml-md-0">
        <?= $this->Html->link('LOGOUT', [
            'controller' => 'users', 
            'action' => 'logout',
            'full_base' => true], [
            'class' => 'text-decoration-none text-dark font-weight-bold',
        ]); ?>
    </div>
</nav>