<main>
    <div class="container-fluid">
        <h2 class="mt-4">Followers</h2>
        <?php 
            // If no followers
            if ($usersfollower->isEmpty()) {
                echo "<p class='text-center'> 
                When people follow you. It will show here. </p>";
            }
        ?>
        <?php foreach ($usersfollower as $userfollower): ?>
            <div class="following mt-4">
                <!-- User data -->
                <?= $this->Html->image('uploads/' . $userfollower->follower_user->profile_picture, [
                    'alt' => 'Profile Picture', 'class' => 'thumb-img-post']
                ); ?>
                <strong>
                    <?= $this->Html->link($userfollower->follower_user->full_name, ['controller' => 'users','action' => 'view', $userfollower->follower_user->id], ['class' => 'text-decoration-none text-dark']) ?>
                </strong>
                <span><?= "@" . $userfollower->follower_user->username; ?></span>
                <!-- Check if logged in user follows certain user --> 
                <?php 
                    $button = "follow";
                    foreach ($usersfollowing as $userfollowing) {
                        if ($userfollowing->user_id == $userfollower->follower_user->id) {
                            $button = "unfollow";
                        break;
                        } 
                        if ($userfollowing->user_id != $userfollower->follower_user->id) {
                            $button = "follow";
                        }
                    }
                ?>
                <!-- Button -->
                <span class="follow<?= $userfollower->follower_user->id ?>">
                    <?php if ($button == "follow"): ?>
                        <?= $this->Html->link('Follow', [
                            'controller' => 'followers', 'action' => 'add', 
                            $userfollower->follower_user->id], ['data-id' => $userfollower->follower_user->id, 'class' => 'add-follow btn btn-primary btn-sm float-right mr-2 mb-2']
                        ); ?>
                    <?php endif; ?>
                    <?php if ($button == "unfollow"): ?>
                        <?= $this->Html->link('Unfollow', [
                            'controller' => 'followers', 'action' => 'delete',  
                            $userfollower->follower_user->id], ['data-id' =>  $userfollower->follower_user->id,'class' => 'delete-follow btn btn-primary active btn-sm float-right mr-2 mb-2']
                        ); ?>
                    <?php endif; ?>
                </span>
            </div><hr>
        <?php endforeach; ?>
    </div>
</main>
