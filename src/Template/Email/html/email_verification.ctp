<?php use Cake\Routing\Router; ?>
<!-- Email Verification Template -->
<p>Hi <?php echo strtok($name, " ") . ","; ?></p></br>
<p>Welcome to MICROBLOG!</p>
<p>Please click the link below to activate your account:</p><br>
    <?= Router::url([
        'controller' => 'Users', 
        'action' => 'activate', 
        $code_of_activation
    ], true) ?> 
<p>Thanks,</p>
<p>Microblog Family</p>