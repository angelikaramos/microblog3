<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class CommentsTable extends Table
{
    public function initialize(array $config)
    {
        // Association
        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'commenter_user_id'
        ]);

        // Enable timestamp behavior
        $this->addBehavior('Timestamp');
    }
}