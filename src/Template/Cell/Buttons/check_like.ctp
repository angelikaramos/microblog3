<?php if ($liked > 0): ?>
    <?= $this->Html->link('<span class="far fa-thumbs-up"></span><span class="sr-only">',['controller' => 'likes', 'action' => 'delete', $post_id] , ['escape' => false, 'data-post' => $post_id, 'class' => 'delete-like text-decoration-none text-primary']); ?>
<?php endif; ?>
<?php if ($liked < 1): ?>
    <?= $this->Html->link('<span class="far fa-thumbs-up"></span><span class="sr-only">',['controller' => 'likes', 'action' => 'add', $post_id] , ['escape' => false, 'data-post' => $post_id, 'class' => 'add-like text-decoration-none text-dark']); ?>
<?php endif; ?>