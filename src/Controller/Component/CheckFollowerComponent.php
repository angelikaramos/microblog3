<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class CheckFollowerComponent extends Component
{
    /**
     * Check if follower data exist
     * @param $user_id, $follower_user_id
     * @return object data
     */
    public function checkFollowerIfExisting($user_id, $follower_user_id)
    {
        // Followers Table
        $followersTable = TableRegistry::getTableLocator()->get('Followers');

        // Check if follower data already exists
        $follower = $followersTable->find('all', [
            'conditions' => [
                'AND' => [
                    'user_ids' => $user_id,
                    'follower_user_id' => $follower_user_id,
                    '_is_deleted' => [1,0],
                ]
            ]
        ]);
        
        return $follower;
    }

    /**
     * Check if logged in user follows certain user
     * @param $user_id
     * @return object data
     */
    public function checkIfFollowing($user_id, $follower_user_id)
    {
        // Followers Table
        $followersTable = TableRegistry::getTableLocator()->get('Followers');

        // Check if following 
        $following = $followersTable->find('all')->where(['user_id' => $user_id, 'follower_user_id' => $follower_user_id])->contain(['FollowerUser'])->first();

        return $following;
    }
}