<main>
    <div class="container-fluid">
        <!-- Edit Profile Button -->
        <?php if ($loggedin_user_id == $user_id): ?>
            <?= $this->Html->Link('Edit Profile', ['controller' => 'users', 
            'action' => 'edit'], ['class' => 'btn btn-primary float-right mt-3']); ?>
        <?php endif; ?>
        <div class="row">
            <!-- User -->
            <div class="col-md-2 mt-4">
            <?= $this->Html->image('uploads/' . $user->profile_picture, [
                'alt' => 'Profile Picture', 'class' => 'thumb-img']
            ); ?>
            </div>
            <div class="col-md-10 mt-5">
                <span class="user-fullname h2"><?= h($user->full_name); ?></span>
                <?= "@" . h($user->username); ?>
                <p class="small"><?= "Joined " . date('F Y', strtotime($user->created)); ?></p>
                <p><b>Posts:</b> <?= $this->cell('Post::countPost', [$user_id]);?>
                | <b>Following:</b> <?= $this->cell('Follower::countFollowing', [$user_id]); ?>
                | <b>Followers:</b> <?= $this->cell('Follower::countFollower', [$user_id]); ?>
                </p>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-12">
                <div class="posts">
                    <!-- If posts is empty -->
                    <?php 
                        if ($posts->isEmpty()) {
                            echo "<p class='text-center'> No posts yet. </p>";
                        }
                    ?>
                    <?php foreach ($posts as $post): ?>
                        <?php $repost_post = false; ?>
                        <?php foreach($reposts as $repost): ?>
                            <?php 
                                if ($post->repost_id == $repost->id) {
                                    $repost_post = true;
                                    break;
                                } 
                                if ($post->repost_id == null) {
                                    $repost_post = false;
                                }   
                            ?>
                        <?php endforeach; ?>
                        <?php if ($repost_post == true): ?>
                            <?php if ($post->post_text == null): ?>
                                <?= $this->element('index_repost', [
                                    'repost_post' => true,
                                    'post_id' => $post->id,
                                    'post_user_id' => $post->user->id,
                                    'post_text' => $post->post_text,
                                    'post_username' => $post->user->username,
                                    'post_repost_id' => $repost->post->repost_id,
                                    'repost_id' => $repost->id,
                                    'repost_repost_id' => $repost->post->repost_id,
                                    'repost_post_id' => $repost->post_id,
                                    'repost_user_id' => $repost->post->user->id, 
                                    'repost_user_profile_picture' => $repost->post->user->profile_picture,
                                    'repost_user_full_name' => $repost->post->user->full_name,
                                    'repost_user_username' => $repost->post->user->username,
                                    'repost_post_text' => $repost->post->post_text,
                                    'repost_post_image' => $repost->post->post_image,
                                    'repost_modified' => $repost->post->modified
                                ]); ?>
                            <?php endif; ?>
                            <?php if ($post->post_text != null): ?>
                                <?= $this->element('index_repost_with_quote', [
                                    'post_id' => $post->id,
                                    'post_text' => $post->post_text,
                                    'post_username' => $post->user->username,
                                    'post_user_id' => $post->user_id,
                                    'post_user_profile_picture' => $post->user->profile_picture,
                                    'post_user_full_name' => $post->user->full_name,
                                    'post_modified' => $post->modified,
                                    'repost_id' => $repost->id,
                                    'repost_post_id' => $repost->post->id,
                                    'repost_user_id' => $repost->post->user->id, 
                                    'repost_user_profile_picture' => $repost->post->user->profile_picture,
                                    'repost_user_full_name' => $repost->post->user->full_name,
                                    'repost_user_username' => $repost->post->user->username,
                                    'repost_post_text' => $repost->post->post_text,
                                    'repost_post_image' => $repost->post->post_image,
                                    'repost_modified' => $repost->post->modified,
                                    'repost_is_deleted' => $repost->post->_is_deleted
                                ]); ?>
                            <?php endif; ?>
                        <?php endif; ?>
                        <?php if ($repost_post == false): ?>
                            <?= $this->element('index_original_post', [
                                'repost_post' => false,
                                'repost_post_text' => $post->post_text,
                                'repost_post_username' => $post->user->username,
                                'post_user_id' => $post->user_id, 
                                'post_id' => $post->id,
                                'post_user_profile_picture' => $post->user->profile_picture,
                                'post_user_full_name' => $post->user->full_name,
                                'post_user_username' => $post->user->username,
                                'post_post_text' => $post->post_text,
                                'post_post_image' => $post->post_image,
                                'post_modified' => $post->modified
                            ]); ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
    </div>
</main>

