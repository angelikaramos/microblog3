<div class="d-inline p-2 bg-info">
    <?= "<span class='text-white'>" . "(X)" . "</span> "; ?>
    <?= $this->Form->postLink('Unrepost', ['controller' => 'reposts', 'action' => 'delete',  $post_id], ['class' => 'text-decoration-none text-white font-weight-bold','confirm' => 'Are you sure you want to undo repost?']) ?>
</div>