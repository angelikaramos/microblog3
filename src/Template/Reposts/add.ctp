<main>
    <div class="container-fluid">
        <div class="post mt-3">
            <div class="post-user mt-2 mb-2">
                <?= $this->Html->image('uploads/' . $user->profile_picture, ['alt' => 'Profile Picture', 'class' => 'thumb-img-post']); ?>
                <?= $this->Html->link($user->full_name, ['controller' => 'users',
                'action' => 'view', $post->id], ['class' => 'text-decoration-none text-dark font-weight-bold']) ?>
                <span><?= "@" . h($user->username); ?></span>
            </div>
            <div class="col-md-12">
                <!-- Reposting original post -->
                <?php if ($post->repost_id == ''): ?>
                    <?php
                        $url = ['controller' => 'reposts', 'action' => 'add', $post->id];
                        echo $this->Form->create(null, [
                            'url' => $url
                        ]);
                    ?>
                <?php endif; ?>
                <!-- Reposting a repost -->
                <?php if ($post->repost_id != ''): ?>
                    <?php if ($post->post_text == null): ?>
                        <?php
                            $url = ['controller' => 'reposts', 'action' => 'add', $post->id];
                            echo $this->Form->create(null, [
                                'url' => $url
                            ]);
                        ?>
                    <?php endif; ?>
                    <?php if ($post->post_text != null): ?>
                        <?php
                            $url = ['controller' => 'reposts', 'action' => 'add', $post->id];
                            echo $this->Form->create(null, [
                                'url' => $url
                            ]);
                        ?>
                    <?php endif; ?>
                <?php endif; ?>
                <?= $this->Form->control('post_text', [
                    'cols' => '20',
                    'rows' => '3',
                    'id' => 'repost_text',
                    'class' => 'form-control',
                    'placeholder' => 'Enter your thoughts here..',
                    'required' => false,
                    'label' => false,
                    'style' => 'border:none;box-shadow:none;',
                ]); ?>
                <!-- Repost -->
                <?php if ($post->repost_id != ''): ?>
                    <?= $this->element('add_repost', [
                        'post_user_profile_picture' => $post->user->profile_picture,
                        'post_user_full_name' => $post->user->full_name,
                        'post_id' => $post->id,
                        'post_user_username' => $post->user->username,
                        'post_image' => $post->post_image,
                        'post_text' => $post->post_text,
                        'post_modified' => $post->modified,
                        'repost' => $repost,
                        'repost_is_deleted' => $repost->is_deleted,
                    ]); ?>
                <?php endif; ?>
                <!-- Original post -->
                <?php if ($post->repost_id == ''): ?>
                    <?php if (empty($repost)): ?>
                        <?= $this->element('add_repost', [
                            'post_user_profile_picture' => $post->user->profile_picture,
                            'post_user_full_name' => $post->user->full_name,
                            'post_id' => $post->id,
                            'post_user_username' => $post->user->username,
                            'post_image' => $post->post_image,
                            'post_text' => $post->post_text,
                            'post_modified' => $post->modified,
                        ]); ?>
                    <?php endif; ?>
                    <?php if (!empty($repost)): ?>
                        <?= $this->element('add_repost', [
                            'post_user_profile_picture' => $post->user->profile_picture,
                            'post_user_full_name' => $post->user->full_name,
                            'post_id' => $post->id,
                            'post_user_username' => $post->user->username,
                            'post_image' => $post->post_image,
                            'post_text' => $post->post_text,
                            'post_modified' => $post->modified,
                            'repost' => $repost,
                            'repost_is_deleted' => $repost->_is_deleted,
                        ]); ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="col-md-2 mt-3 float-right">
                <?= $this->Form->button(__('Repost'), [
                    'class' => 'form-control btn btn-primary',
                    'id' => 'repost'
                ]); ?>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</main>
        
