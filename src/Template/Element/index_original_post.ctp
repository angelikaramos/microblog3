<div class="post">
    <!-- Add dropdown_post element if logged in user is the one who post -->
    <?php 
        if ($loggedin_user_id == $post_user_id) {
            echo $this->element('dropdown_post', ['post_id' => $post_id]); 
        }
    ?>
    <!-- Post user -->
    <div class="post-user">
        <?= $this->Html->image('uploads/' . $post_user_profile_picture, 
        ['alt' => 'Profile Picture', 'class' => 'thumb-img-post']); ?>
        <span class="font-weight-bold">
            <?= $this->Html->link($post_user_full_name, [
                'controller' => 'users',
                'action' => 'view', $post_user_id], [
                'class' => 'text-decoration-none text-dark font-weight-bold']
            ); ?>
        </span>
        <span><?= "@" . h($post_user_username); ?></span>
    </div>
    <!-- Post  -->
    <div class="post ml-5 mb-2">
        <!-- Post text -->
        <p class="post-content"><?= h($post_post_text); ?></p>
        <!-- Post image -->
        <a href="<?= $this->Url->build(['controller' => 'posts', 'action' => 'view', $post_id], ['escape' => false]); ?>" class="text-decoration-none text-dark">    
            <?php if (!empty($post_post_image)): ?>
                <?= $this->Html->image('posts/' . h($post_post_image), [
                    'class' => 'rounded thumb-post-image'
                ]); ?>
            <?php endif; ?>
        </a>
    </div>
    <!-- Like, Comment and Repost Button -->
    <?= $this->element('index_buttons', ['post_id' => $post_id,]); ?>   
    <!-- Post date -->
    <small class="text-muted"><?= date('g:i A · M j, Y', strtotime($post_modified)); ?></small>
</div><hr>