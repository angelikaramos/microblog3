<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class GetUserComponent extends Component
{
    /**
     * Get users not yet followed
     * @param $user_id
     * @return object data
     */
    public function getUsersNotYetFollowed($user_id)
    {
        // Get instance table using TableLocator class
        $this->Followers = TableRegistry::getTableLocator()->get('Followers');
        $this->Users = TableRegistry::getTableLocator()->get('Users');

        // Get the list of user_id that user logged in follows
        $conditions = ['AND' => ['follower_user_id' => $user_id,  '_is_deleted' => 0]];
        $usersFollowed = $this->Followers->find('list', [
            'conditions' => $conditions,
            'fields' => ['user_id']
        ]);

        // Get users where id is not in $usersFollowed and id 
        $usersNotFollowed = $this->Users->find('all',  [
            'conditions' => [
                'id NOT IN' => $usersFollowed,
                '_is_email_activated !=' => '0',
                'id !=' => $user_id],
            'limit' => '2', 
            'order' => 'rand()'
        ]);

        return $usersNotFollowed;
    }

    /**
     * Get users already followed
     * @param $user_id
     * @return object data
     */
    public function getUsersAlreadyFollowed($user_id)
    {
        // Followers Table
        $followersTable = TableRegistry::getTableLocator()->get('Followers');

        // Get all logged in user following
        $following = $followersTable->find('all', [
            'conditions' => [
                'AND' => ['follower_user_id' => $user_id, '_is_deleted' => 0] 
            ]
        ])->contain(['FollowingUser']);

        return $following;
    }
}