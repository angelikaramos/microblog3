<div class="post mt-3">
    <!-- Add dropdown_post element if logged in user is the one who post -->
    <?php 
        if ($loggedin_user_id == $post_user_id) {
            echo $this->element('dropdown_post', ['post_id' => $post_id]); 
        }
    ?>
    <!-- Post user -->
    <div class="post-user mt-2">
        <?= $this->Html->image('uploads/' . $post_user_profile_picture, 
        ['alt' => 'Profile Picture', 'class' => 'thumb-img-post']); ?>
        <span class="font-weight-bold">
            <?= $this->Html->link($post_user_full_name, [
                'controller' => 'users',
                'action' => 'view', $post_user_id], [
                'class' => 'text-decoration-none text-dark font-weight-bold']
            ); ?>
        </span>
        <span><?= "@" . h($post_user_username); ?></span>
    </div>
    <!-- Post -->
    <div class="post ml-5">
        <?php if (empty($post_post_image)): ?>
            <!-- Post text -->
            <p class="post-content custom-select-lg"><?= h($post_post_text); ?></p>
        <?php endif; ?>
        <?php if (!empty($post_post_image)): ?>
            <!-- Post text -->
            <p class="post-content"><?= h($post_post_text); ?></p>
            <!-- Post image -->
            <?= $this->Html->image('posts/' . h($post_post_image), [
                'style' => 'width:500px'
            ]); ?>
        <?php endif; ?>
    </div>
    <!-- Post date -->
    <small class="text-muted"><?= date('g:i A · M, j Y', strtotime($post_modified)); ?></small>
    <hr>
    <!-- Buttons -->
    <?= $this->element('view_buttons', ['post_id' => $post_id]); ?>
</div><hr>