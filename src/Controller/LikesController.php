<?php

namespace App\Controller;

class LikesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
    }

    /**
     * Add like
     * @param $post_id
     * @return redirected page
     */
    public function add(int $post_id)
    {
        // Get logged in user_id
        $liker_user_id = $this->Auth->user('id');

        // Check if data already existing
        $checkLike = $this->Likes->findAllByPost_idAndLiker_user_id($post_id, $liker_user_id);

        // Update column deleted and _is_deleted if data already existing
        if (!$checkLike->isEmpty()) {
            // Get like data then set deleted and _is_deleted value
            $like = $this->Likes->find('all', [
                'conditions' => [
                    'post_id' => $post_id, 
                    'liker_user_id' => $liker_user_id, 
                    '_is_deleted' => 1
                ]
            ])->first();
            
            $like = $this->Likes->get($like->id);
            $like->deleted = null;
            $like->_is_deleted = 0;
            if ($this->Likes->save($like)) {
                // Handle ajax
                if ($this->request->is('ajax')) {
                    $this->set('loggedin_user_id', $liker_user_id);
                    $this->set('post_id', $post_id);
                    $this->render('btn_liked', 'ajax');
                } else {
                    return $this->redirect($this->referer());
                }
            }
        }

        // Add data if not yet already existing
        if ($checkLike->isEmpty()) {
            $like = $this->Likes->newEntity();
            
            // Set post_id and liker_user_id then save
            $like->post_id = $post_id;
            $like->liker_user_id = $liker_user_id;
            $like->_is_deleted = 0;
            if ($this->Likes->save($like)) {
                // Handle ajax
                if ($this->request->is('ajax')) {
                    $this->set('loggedin_user_id', $liker_user_id);
                    $this->set('post_id', $post_id);
                    $this->render('btn_liked', 'ajax');
                } else {
                    return $this->redirect($this->referer());
                }
            }
        }       
    }

    /**
     * Delete like
     * @param $post_id
     * @return redirected page
     */
    public function delete(int $post_id)
    {
        // Get logged in user_id
        $liker_user_id = $this->Auth->user('id');

        // Get like data
        $like = $this->Likes->find('all', [
            'conditions' => ['post_id' => $post_id, 'liker_user_id' => $liker_user_id, '_is_deleted' => 0]
        ])->first();
        $like = $this->Likes->get($like->id);

        // Set and save for - soft delete
        $like->deleted = date('Y-m-d H:i:s');
        $like->_is_deleted = 1;
        if ($this->Likes->save($like)) {
            // Handle ajax
            if ($this->request->is('ajax')) {
                $this->set('loggedin_user_id', $liker_user_id);
                $this->set('post_id', $post_id);
                $this->render('btn_liked', 'ajax');
            } else {
                return $this->redirect($this->referer());
            }
        }
    }
}