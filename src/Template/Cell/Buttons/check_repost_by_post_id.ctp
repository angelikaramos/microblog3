<?php if ($reposted > 0): ?>
    <?= $this->Form->postLink('<span class="fa fa-retweet"></span><span class="sr-only">', ['controller' => 'reposts', 'action' => 'delete',  $reposted_post_id], ['escape' => false,'class' => 'text-decoration-none text-info','confirm' => 'Are you sure you want to undo repost?']) ?>
<?php endif; ?>
<?php if ($reposted < 1): ?>
    <?= $this->Html->link('<span class="fa fa-retweet"></span><span class="sr-only">',[
    'controller' => 'reposts', 'action' => 'add',  $post_id], ['escape' => false, 'class' => 'text-decoration-none text-dark']) ?>
<?php endif; ?>