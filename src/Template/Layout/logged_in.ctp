<!DOCTYPE html>
<html lang="en">
	<head>
        <?= $this->Html->charset() ?>
		<title>
            <?php $this->assign('title', 'MICROBLOG'); ?>
			<?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <!-- CSS files -->
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('all.min.css') ?>
        <?= $this->Html->css('styles.css') ?>
        
        <!-- JavaScript files -->
        <?= $this->Html->script('jquery.js') ?>
        <?= $this->Html->script('navigation.js') ?>
        <?= $this->Html->script('jquery.min.js', ['defer']); ?>
        <?= $this->Html->script('bootstrap.bundle.min.js', ['defer']) ?>
        <?= $this->Html->script('popper.min.js', ['defer']) ?>
        <?= $this->Html->script('script.js', ['defer']) ?>

        <!-- Disabled button for empty inputs -->
        <?= $this->Html->script('disabled.js', ['defer']) ?>

        <!-- For AJAX requests -->
        <?= $this->Html->script('like.js', ['defer']) ?>
        <?= $this->Html->script('follow.js', ['defer']) ?>
        
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
	</head>
	<body>
        <!-- Navigation Bar -->
        <?= $this->element('navbar') ?>
		<div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <!-- Side Navigation Bar -->
                <?= $this->element('side_navbar') ?>
            </div>
			<div id="layoutSidenav_content">
                <!-- Flash message -->
                <?= $this->Flash->render() ?>
                <!-- Main Content -->
                <?= $this->fetch('content') ?>
                <!-- Pagination -->
                <?= $this->element('pagination') ?>
                <!-- Footer -->
                <?= $this->element('footer') ?>
            </div>
        </div>
	</body>
</html>