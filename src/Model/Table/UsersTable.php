<?php

namespace App\Model\Table;

use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\RulesChecker;
use Cake\ORM\Rule\IsUnique;

class UsersTable extends Table
{
    public function initialize(array $config)
    {
        // Association
        $this->hasMany('Posts');
        $this->hasMany('Reposts');
        $this->hasMany('Followers');
        $this->hasMany('Likes');
        $this->hasMany('Comments');
        
        // Enable timestamp behavior
        $this->addBehavior('Timestamp');
    }

    // Validations 
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('full_name', [
                'notEmpty' => [
                    'rule' => 'notBlank',
                    'message' => 'Please enter your full name.'
                ]
            ])
            ->add('email', [
                'notEmpty' => [
                    'rule' => 'notBlank',
                    'message' => 'Please enter your email.'
                ]
            ])
            ->add('email', 'validFormat', [
                'rule' => 'email',
                'message' => 'Email must be valid.'
            ])
            ->add('username', [
                'notEmpty' => [
                    'rule' => 'notBlank',
                    'message' => 'Please enter your username.'
                ],
                'alphaNumeric'=> [
                    'rule' => 'alphaNumeric',
                    'message' => 'Username must contain only letters and numbers.'
                ],
                'minLength' => [
                    'rule' => ['minLength', 5],
                    'message' => 'Username must be at least 5 characters long.'
                ],
                'maxLength' => [
                    'rule' => ['maxLength', 15],
                    'message' => 'Username must be not more than 15 characters long.'
                ]
            ])
            ->add('password', [
                'notEmpty' => [
                    'rule' => 'notBlank',
                    'message' => 'Please enter your password.'
                ],
                'alphaNumeric'=> [
                    'rule' => 'alphaNumeric',
                    'message' => 'Password can only be letters and numbers.'
                ],
                'minLength' => [
                    'rule' => ['minLength', 8],
                    'message' => 'Password must be at least 8 characters long.'
                ]
            ])
            ->allowEmpty('profile_picture')
            ->add('profile_picture', [
                'validExtension' => [
                    'rule' => ['extension', ['jpeg', 'png', 'jpg']],
                    'message' => __('Please upload image with .png, .jpg and .jpeg extension only.'),
                ],
            ]);
            
        return $validator;
    }

    // Validation for username
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username'], 'Username has already been used.'));

        return $rules;
    }

    // Validation for username and password in login page
    public function validationLogin(Validator $validator)
    {
        $validator
            ->notEmpty('username', 'Please enter your username.')
            ->notEmpty('password', 'Please enter your password.');
        return $validator;
    }
}