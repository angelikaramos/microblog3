<main>
    <div class="container-fluid">
        <h2 class="mt-4">Following</h2>
        <?php 
            // If no following
            if ($usersfollowing->isEmpty()) {
                echo "<p class='text-center'> Start following people and see their posts. </p>";
            }
        ?>
        <?php foreach ($usersfollowing as $userfollow): ?>
            <!-- User data -->
            <div class="following mt-4">
                <?= $this->Html->image('uploads/' . $userfollow->following_user->profile_picture, [
                    'alt' => 'Profile Picture', 'class' => 'thumb-img-post']
                ); ?>
                <strong>
                    <?= $this->Html->link($userfollow->following_user->full_name, ['controller' => 'users', 'action' => 'view', $userfollow->user_id], ['class' => 'text-decoration-none text-dark']); ?>
                </strong>
                <span><?= "@" . $userfollow->following_user->username; ?></span>
                <!-- Button -->
                <span class="follow<?=$userfollow->user_id ?>">
                    <?= $this->Html->link('Unfollow', ['controller' => 'followers', 
                    'action' => 'delete',  $userfollow->user_id], ['data-id' => $userfollow->user_id,'class' => 'delete-follow btn btn-primary active btn-sm float-right mr-2 mb-2']); ?>
                </span>
            </div><hr>
        <?php endforeach; ?>
    </div>
</main>
