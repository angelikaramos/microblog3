<main>
    <div class="container-fluid">
        <h2 class="mt-4">Who to follow</h2>
        <?php 
            // If no followers
            if ($follows->isEmpty()) {
                echo "<p class='text-center'> Users you may be interested in will show here. </p>";
            }
        ?>
        <?php foreach ($follows as $follow): ?>
            <div class="following mt-4">
                <!-- User data -->
                <?= $this->Html->image('uploads/' . $follow->profile_picture, [
                    'alt' => 'Profile Picture', 'class' => 'thumb-img-post']
                ); ?>
                <strong>
                    <?= $this->Html->link($follow->full_name, ['controller' => 'users','action' => 'view', $follow->id], ['class' => 'text-decoration-none text-dark']) ?>
                </strong>
                <span><?= "@" . $follow->username; ?></span>
                <!-- Check if logged in user follows certain user --> 
                <?php 
                    $button = "follow";
                    foreach ($usersfollowing as $userfollowing) {
                        if ($userfollowing->user_id == $follow->id) {
                            $button = "unfollow";
                        break;
                        } 
                        if ($userfollowing->user_id != $follow->id) {
                            $button = "follow";
                        }
                    }
                ?>
                <!-- Button -->
                <span class="follow<?= $follow->id ?>">
                    <?php if ($button == "follow"): ?>
                        <?= $this->Html->link('Follow', [
                            'controller' => 'followers', 'action' => 'add', 
                            $follow->id], ['data-id' => $follow->id, 'class' => 'add-follow btn btn-primary btn-sm float-right mr-2 mb-2']
                        ); ?>
                    <?php endif; ?>
                    <?php if ($button == "unfollow"): ?>
                        <?= $this->Html->link('Unfollow', [
                            'controller' => 'followers', 'action' => 'delete',  
                            $userfollowing->id], ['data-id' => $follow->id,'class' => 'delete-follow btn btn-primary active btn-sm float-right mr-2 mb-2']
                        ); ?>
                    <?php endif; ?>
                </span>
            </div><hr>
        <?php endforeach; ?>
    </div>
</main>
