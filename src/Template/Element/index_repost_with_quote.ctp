<div class="post">
    <!-- Add dropdown_post element if logged in user is the one who post -->
    <?php 
        if ($loggedin_user_id == $post_user_id) {
            echo $this->element('dropdown_post', ['post_id' => $post_id]); 
        }
    ?>
    <!-- Post user -->
    <div class="post-user">
        <?= $this->Html->image('uploads/' . $post_user_profile_picture, 
        ['alt' => 'Profile Picture', 'class' => 'thumb-img-post']); ?>
        <span class="font-weight-bold">
            <?= $this->Html->link($post_user_full_name, [
                'controller' => 'users',
                'action' => 'view', $post_user_id], [
                'class' => 'text-decoration-none text-dark font-weight-bold']
            ); ?>
        </span>
        <span><?= "@" . h($post_username); ?></span>
    </div>
    <!-- Post text -->
    <span class="post-content ml-5"><?= h($post_text); ?></span>
    <?php if ($repost_is_deleted == 1): ?>
        <?= $this->element('url_post', ['repost_post_id' => $repost_post_id]); ?>
    <?php endif; ?>
    <!-- Repost Post -->
    <div class="card mt-2 ml-5">
        <?php if ($repost_is_deleted == 1): ?>
            <div class="card-header">
                <p class="text-muted">This post is unavailable.</p>
            </div>
        <?php endif; ?>
        <?php if ($repost_is_deleted == 0): ?>
            <div class="card-header">
                <!-- Profile picture -->
                <?= $this->Html->image('uploads/' . $repost_user_profile_picture, 
                ['alt' => 'Profile Picture', 'class' => 'thumb-img-post']); ?>
                <!-- Poster full name -->
                <span class="font-weight-bold">
                    <?= $this->Html->link(
                        $repost_user_full_name, [
                        'controller' => 'users',
                        'action' => 'view', $repost_user_id], [
                        'class' => 'text-decoration-none text-dark font-weight-bold']
                    ); ?>
                </span>
                <!-- Poster username -->
                <span><?= "@" . h($repost_user_username); ?></span>
                <!-- Post date -->
                <span class="card-text">
                    <small class="text-muted">
                        <?= "· " . date('M d', strtotime($repost_modified)); ?>
                    </small>
                </span>
            </div>
            <a href="<?= $this->Url->build(['controller' => 'posts', 'action' => 'view', $repost_post_id], ['escape' => false]); ?>" class="text-decoration-none text-dark">
                <div class="card-body">
                    <!-- Post -->
                    <div class="post ml-5 mb-2">
                        <?php if (empty($repost_post_image)): ?>
                            <!-- Post text -->
                            <span class="post-content"><?= h($repost_post_text); ?></span>
                        <?php endif; ?>
                        <?php if (!empty($repost_post_image)): ?>
                            <!-- Post text -->
                            <p class="post-content"><?= h($repost_post_text); ?></p>
                            <a href="<?= $this->Url->build(['controller' => 'posts', 'action' => 'view', $repost_post_id], ['escape' => false]); ?>" class="text-decoration-none text-dark">  
                                <!-- Post image -->  
                                <?= $this->Html->image('posts/' . h($repost_post_image), [
                                    'class' => 'rounded thumb-post-image'
                                ]); ?>
                            </a>
                        <?php endif; ?>
                        <!-- Check if it is also reposted quote, if true get post_id then create url -->
                        <?= $this->cell('Post::getPostId', [$repost_id]); ?>
                    </div>
                </div>
            </a>
        <?php endif; ?>
    </div><br>
    <!-- Like, Comment and Repost Button -->
    <?= $this->element('index_buttons', [
        'post_text' => $post_text,
        'post_id' => $post_id,
        'repost_id' => $repost_id,
        'repost_post_id' => $repost_post_id]
    ); ?>
    <!-- Post date -->
    <small class="text-muted"><?= date('g:i A · M j, Y', strtotime($post_modified)); ?></small>
</div><hr>