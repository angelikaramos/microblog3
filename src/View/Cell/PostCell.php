<?php

namespace App\View\Cell;

use Cake\View\Cell;

class PostCell extends Cell
{
    /**
     * Count posts
     * @param $user_id
     * @return int
     */
    public function countPost($user_id)
    {
        $this->loadModel('Posts');
        $posts = $this->Posts->find('all', [
            'conditions' => ['user_id' => $user_id, '_is_deleted' => 0, 'repost_id is' => null]
        ]);

        $this->set('countPost', $posts->count());
    }

    /**
     * Get post id of reposted post
     * @param $post_id
     * @return object data
     */
    public function getPostId($repost_id)
    {
        // Load Model
        $this->loadModel('Posts');
        $this->loadModel('Reposts');

        $repost = $this->Reposts->find('all', [
            'conditions' => ['id' => $repost_id],
            'fields' => ['post_id']
        ])->first();

        // If there's another repost
        if (!empty($repost)) {
            $post = $this->Posts->find('all', [
                'conditions' => ['id' => $repost->post_id],
                'fields' => 'repost_id'
            ])->first();

            if (!empty($post)) {
                $repost = $this->Reposts->find('all', [
                    'conditions' => ['id' => $post->repost_id],
                    'fields' => 'post_id'
                ])->first();
                
                // If there's another repost
                if (!empty($repost)) {
                    // Set post_id
                    $this->set('post_id', $repost->post_id);
                }

                if (empty($repost)) {
                    // Set post_id
                    $this->set('post_id', null);
                }
            }

            if (empty($post)) {
                // Set post_id
                $this->set('post_id', null);
            }
        }

        if (empty($repost)) {
            // Set post_id
            $this->set('post_id', null);
        }
    }
}