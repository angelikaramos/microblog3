<div class="post mt-3">
    <!-- Add dropdown_post element if logged in user is the one who post -->
    <?php 
        if ($loggedin_user_id == $post_user_id) {
            echo $this->element('dropdown_post', ['post_id' => $post_id]); 
        }
        // If post_text is equal to null (reposted post without a quote) the post only will be displayed with repost icon
        if ($repost_post_text == null) {
            if ($loggedin_user_id == $post_user_id) {
                // Repost label
                echo "<span class='small text-primary'>";
                echo "<i class='fa fa-retweet mr-1' aria-hidden='true'></i>";
                echo "You reposted";
                echo "</span>";
            }
            if ($loggedin_user_id != $post_user_id) {
                // Repost label
                echo "<span class='small text-primary'>";
                echo "<i class='fa fa-retweet mr-1' aria-hidden='true'></i>";
                echo "Reposted by " . $repost_post_username;
                echo "</span>";
            }
        }
    ?>
    <!-- Post user -->
    <div class="post-user mt-2">
        <?= $this->Html->image('uploads/' . $post_user_profile_picture, 
        ['alt' => 'Profile Picture', 'height' => '50px']); ?>
        <span class="font-weight-bold">
            <?= $this->Html->link(
                $post_user_full_name, array(
                'controller' => 'users',
                'action' => 'view', $post_id), array(
                'class' => 'text-decoration-none text-dark font-weight-bold')
            ); ?>
        </span>
        <span><?= "@" . h($post_user_username); ?></span>
    </div>
    <!-- Post text -->
    <p class="post-content ml-5"><?php echo h($post_post_text); ?></p>
    <!-- Post date -->
    <small class="text-muted"><?php echo date('g:i A · M, j Y', strtotime($post_modified)); ?></small>
</div><hr>