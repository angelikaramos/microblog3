<main>
    <div class="container-fluid">
        <!-- Commenter user -->
        <div class="post-user mt-3">
            <?= $this->Html->image('uploads/' . $comment->user->profile_picture, [
            'alt' => 'Profile Picture', 'class' => 'thumb-img-post']); ?>
            <span class="font-weight-bold"><?= h($comment->user->full_name); ?></span>
            <span><?= "@" . h($comment->user->username); ?></span>
        </div>
        <div class="row">
            <!-- Edit Comment -->  
            <div class="col-md-10 mt-3">
                <?= $this->Form->create($comment, [
                    'url' => [$comment->id]
                ]); ?>
                <?= $this->Form->control('comment_text', [
                    'cols' => '25',
                    'rows' => '5',
                    'id' => 'edit_comment_text',
                    'class' => 'form-control mb-3',
                    'label' => false,
                ]); ?>
            </div>
            <div class="col-md-2 mt-5">
                <?= $this->Form->button(__('Save'), [
                    'class' => 'form-control btn btn-primary',
                    'id' => 'edit_comment'
                ]); ?>
                <?= $this->Form->end(); ?>
            </div>
        </div>
    </div>
</main>

