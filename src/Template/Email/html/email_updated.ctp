<!-- Template for Email Verification when email was updated -->
<?php use Cake\Routing\Router; ?>
<p>Hi <?php echo strtok($name, " ") . ","; ?></p></br>
<p>Thank you for using MICROBLOG!</p>
<p>Please click the link below to activate your account:</p>
    <?= Router::url([
        'controller' => 'Users', 
        'action' => 'activate', 
        $code_of_activation
    ], true) ?> 
<p>Thanks,</p>
<p>Microblog Family</p>