<?php

namespace App\Model\Table;

use Cake\ORM\Table;

class LikesTable extends Table
{
    public function initialize(array $config)
    {
        // Association
        $this->belongsTo('Users', [
            'className' => 'Users',
            'foreignKey' => 'liker_user_id'
        ]);
        $this->belongsTo('Posts', [
            'className' => 'Posts',
            'foreignKey' => 'post_id'
        ]);
        
        // Enable timestamp behavior
        $this->addBehavior('Timestamp');
    }
}