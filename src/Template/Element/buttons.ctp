<div class="btn-group-sm float-right">
    <!-- Like -->
    <div class="d-inline p-2">
        <?= $this->cell('Buttons::checkLike', [$post_id, $loggedin_user_id]) ?>
        <?= $this->cell('Buttons::countLike', [$post_id]) ?>
    </div>
    <!-- Comment -->
    <div class="d-inline p-2">
        <?= $this->Form->postLink('<span class="far fa-comment"></span><span class="sr-only">', ['controller' => 'posts', 
        'action' => 'view',  $post_id], ['escape' => false, 'class' => 'text-decoration-none text-dark']) ?>
        <?= $this->cell('Buttons::countComment', [$post_id]) ?>
    </div>
    <!-- Repost -->
    <div class="d-inline p-2">
        <!-- If original post -->
        <?php if (empty($repost_id)): ?>
            <?php if($loggedin_user_id == $post_user_id): ?>
                <?= $this->cell('Buttons::checkRepostByPostId', [$post_id, $loggedin_user_id]) ?>
                <?= $this->cell('Buttons::countRepost', [$post_id]) ?>
            <?php endif; ?>
            <?php if($loggedin_user_id != $post_user_id): ?>
                <?= $this->Html->link('<span class="fa fa-retweet"></span><span class="sr-only">',[
                'controller' => 'reposts', 'action' => 'add',  $post_id], ['escape' => false, 'class' => 'text-decoration-none text-dark']) ?>
                <?= $this->cell('Buttons::countRepost', [$post_id]) ?>
            <?php endif; ?>
        <?php endif; ?>
        <!-- If reposted post -->
        <?php if (!empty($repost_id)): ?>
            <?php if (!empty($post_text)): ?>
                <?= $this->Html->link('<span class="fa fa-retweet"></span><span class="sr-only">',[
                'controller' => 'reposts', 'action' => 'add',  $post_id], ['escape' => false, 'class' => 'text-decoration-none text-dark']) ?>
                <?= $this->cell('Buttons::countRepost', [$post_id]) ?>
            <?php endif;?>
            <?php if (empty($post_text)): ?>
                <?= $this->cell('Buttons::checkRepostById', [$repost_id, $loggedin_user_id, $post_id]) ?>
                <?= $this->cell('Buttons::countRepost', [$repost_post_id]) ?>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>