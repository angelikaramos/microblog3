<?php foreach ($comments as $comment) : ?>
    <div class="flex-row comment-row">
        <!-- Dropdown -->
        <?php if ($loggedin_user_id == $comment->user->id): ?>
            <?= $this->element('dropdown_comment', ['comment_id' => $comment->id]); ?>
        <?php endif; ?>
        <?= $this->Html->image('uploads/' . $comment->user->profile_picture, ['alt' => 'Profile Picture', 'class' => 'thumb-img-post']); ?>
        <span class="font-weight-bold">
            <?= $this->Html->link($comment->user->full_name, ['controller' => 'users', 'action' => 'view', $comment->user->id], ['class' => 'text-decoration-none text-dark font-weight-bold']); ?>
        </span>
        <span><?= "@" . h($comment->user->username); ?></span>
        <p class="card-text ml-5"><?= h($comment->comment_text); ?></p>
        <small class="text-muted">
            <?= date('g:i A - j M Y', strtotime( $comment->modified)); ?>
        </small><hr>
    </div>
<?php endforeach; ?>