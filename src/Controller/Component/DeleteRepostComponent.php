<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class DeleteRepostComponent extends Component
{
    /**
     * Delete repost without qoute
     * @param $id
     * 
     */
    public function deleteRepostWithoutQuote($id)
    {
        // Get instance table using TableLocator class
        $this->Reposts = TableRegistry::getTableLocator()->get('Reposts');
        $this->Posts = TableRegistry::getTableLocator()->get('Posts');

        $ids = $this->Reposts->find('list', [
            'conditions' => ['post_id' => $id],
            'fields' => ['id']
        ]);

        // Set and save for - soft delete of repost data, if this post_id was reposted
        foreach ($ids as $id) {
            $repost = $this->Reposts->get($id);
            $repost->_is_deleted = 1;
            $repost->deleted = date('Y-m-d H:i:s');
            $this->Reposts->save($repost);
        }

        $ids = $this->Reposts->find('list', [
            'conditions' => ['_is_deleted' => 1],
            'fields' => ['id']
        ]);

        // Set and save for - soft delete of post data, if post data is a reposted without quote
        foreach ($ids as $id) {
            $post = $this->Posts->find('all', [
                'conditions' => ['repost_id' => $id, 'post_text' => '']
            ])->first();
            if (!empty($post)) {
                $post->_is_deleted = 1;
                $post->deleted = date('Y-m-d H:i:s');
                $this->Posts->save($post);
            }
        }
    }
}