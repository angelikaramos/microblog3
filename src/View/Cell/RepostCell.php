<?php

namespace App\View\Cell;

use Cake\View\Cell;

class RepostCell extends Cell
{
    /**
     * Get repost of certain reposted post
     * @param $post_id
     * @param object data or null
     */
    public function getRepost($post_id, $post_repost_id)
    {
        // Load Model
        $this->loadModel('Posts');
        $this->loadModel('Reposts');

        $repost = $this->Reposts->find('all', [
            'conditions' => ['post_id' => $post_id]
        ])->first();

        if (!empty($repost)) {
            $post = $this->Posts->find('all', [
                'conditions' => ['Posts.id' => $post_id],
                'contain' => ['Users']
            ])->first();
            $repost = $this->Reposts->find('all', [
                'conditions' => ['Reposts.id' => $post->repost_id],
                'contain' => ['Users']
            ])->first();
            
            $this->set('post_repost_id', $post_repost_id);
            $this->set('post', $post);

            if (!empty($repost)) {
                $post = $this->Posts->find('all', [
                    'conditions' => ['Posts.id' => $repost->post_id],
                    'contain' => ['Users']
                ])->first();
                
                $this->set('repost_is_deleted', $post->is_deleted);
                $this->set('post_repost_id', $post_repost_id);
                $this->set('post', $post);
            }

            if (empty($repost)) {
                $this->set('post', null);
            }
        }

        if (empty($repost)) {
            $this->set('post', null);
        }
    }
}