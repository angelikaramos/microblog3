$(document).ready(function () {
    // Add follow
    $(".add-follow").unbind().bind('click', function(event){
        event.preventDefault();
        // Get user_id 
        var user_id = this.dataset.id;
        var follow = ".follow"+user_id;
        // Prevent user to double send request
        var alreadyClicked = $(this).data('clicked');
        if (alreadyClicked) {
            return false;
        }
        $(this).data('clicked', true);
        $.ajax({
            async:true, 
            dataType:"html", 
            success:function (data, textStatus) {
                $(follow).html(data);
                $(this).data('clicked', false);
            }, 
            url:$(this).attr('href')
        });
        return false;
    });

    // Delete follow
    $(".delete-follow").unbind().bind('click', function(event){
        event.preventDefault();
        var response = confirm("Are you sure you want to unfollow this person?");
        if (response == true) {
            // Get user_id 
            var user_id = this.dataset.id;
            var follow = ".follow"+user_id;
            // Prevent user to double send request
            var alreadyClicked = $(this).data('clicked');
            if (alreadyClicked) {
                return false;
            }
            $(this).data('clicked', true);
            $.ajax({
                async:true, 
                dataType:"html", 
                success:function (data, textStatus) {
                    $(follow).html(data);
                    $(this).data('clicked', false);
                }, 
                url:$(this).attr('href')
            });
            return false;
        }
    });
});