<?php

namespace App\Controller;

use Cake\Event\Event;
use Cake\Routing\Router;

class PostsController extends AppController
{
    /**
     * Executed before every action
     * 
     * @return variable or object data
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Component
        $this->loadComponent('Paginator');
        $this->loadComponent('GetUser');

        // Load Model
        $this->loadModel('Users');

        // Get logged in user_id
        $user_id = $this->Auth->user('id');
        $this->set('loggedin_user_id', $user_id);

        // Get logged in user full_name for side_navbar
        if ($this->Auth->user()) {
            $user = $this->Users->find('all', [
                'conditions' => ['id' => $user_id],
                'fields' => ['full_name'],
            ])->first();
        }
        $this->set('full_name', $user->full_name);

        // Get users not yet followed for side_navbar
        $usersNotfollowed = $this->GetUser->getUsersNotYetFollowed($user_id);
        $this->set('usersNotFollowed', $usersNotfollowed);
    }

    /**
     * Display posts
     * 
     * @return object data
     */
    public function index()
    {
        // Set layout
        $this->viewBuilder()->setLayout('logged_in');

        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        // Load Model
        $this->loadModel('Followers');
        $this->loadModel('Reposts');

        // Get user_id's of logged in user follows
        $following = $this->Followers->find('list', [
            'conditions' => ['AND' => ['follower_user_id' => $user_id, '_is_deleted' => 0]],
            'fields' => ['user_id']
        ]);

        // Get reposts of logged in user without quote
        $reposts = $this->Posts->find('list', [
            'conditions' => ['user_id' => $user_id, 'post_text' => "", 'repost_id IS NOT' => null],
            'fields' => ['id']
        ]);

        $settings = [
            'conditions' => ['Posts._is_deleted' => 0, 'Posts.id NOT IN' => $reposts, 'OR' => ['Posts.user_id' => $user_id, 'Posts.user_id IN' => $following]], 
            'order' => ['modified' => 'desc'],
            'limit' => '2',
            'contain' => ['Users']
        ];

        // Get reposts
        $reposts = $this->Reposts->find('all')->contain(['Posts' => ['Users'], 'Users']);
        $posts = $this->Paginator->paginate($this->Posts, $settings);

        $this->set('reposts', $reposts);
        $this->set(compact('posts'));
    }

    /**
     * Add post
     * 
     * @return redirected page 
     */
    public function add()
    {
        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        $post = $this->Posts->newEntity();
        if ($this->request->is('post')) {
            // Check validation for more than 140 characters
            $post = $this->Posts->newEntity($this->request->getData(), ['validate' => 'default']);
            if ($post->getErrors()) {
                // Flash message
                $this->Flash->error(__('Post must be only 140 characters long.'));
                return $this->redirect(['action' => 'index']);
            }

            // Get data from form
            $data = $this->request->getData();

            // Check if uploaded post_image is more than 10mb
            if ($data['post_image']['size'] >= 10000000) {
                // Flash message
                $this->Flash->error(__(
                    'Unable to create post. Image must be less than 10MB.'
                ));
                return $this->redirect($this->referer());
            }

            // Check validation for post_image
            $post = $this->Posts->newEntity($this->request->getData(), ['validate' => 'upload']);
            if ($post->getErrors()) {
                // Flash message
                $this->Flash->error(__('Please upload image with .png, .jpg and .jpeg extension only.'));
                return $this->redirect(['action' => 'index']);
            }

            // If post_image is not empty, upload image and set post_image
            if (!empty($data['post_image']['name'])) {
                $tmp = $data['post_image']['tmp_name']; 
                $image = $data['post_image']['name'];
                $target = WWW_ROOT.'img'.DS.'posts'.DS; 
                $target = $target.basename($image); 
                move_uploaded_file($tmp, $target);
                $data['post_image'] = $data['post_image']['name'];
            }

            $post = $this->Posts->patchEntity($post, $data);
            $post->user_id = $user_id;
            $post->_is_deleted = 0;
            if ($this->Posts->save($post)) {
                // Flash message
                $this->Flash->success(__('You created a post.'));
                return $this->redirect(['action' => 'index']);
            }
            // Flash message
            $this->Flash->error(__('Unable to create post.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->set('post', $post);
    }

    /**
     * View post
     * @param $id
     * @return object data
     */
    public function view($post_id)
    {
        // Set layout
        $this->viewBuilder()->setLayout('logged_in_form');

        // Load Model
        $this->loadModel('Reposts');

        // Get post data by id
        $post = $this->Posts->find('all', [
            'conditions' => ['Posts.id' => $post_id, '_is_deleted' => 0]
        ])->contain(['Users'])->first();

        // If empty, then it is soft deleted
        if (empty($post)) {
            $this->Flash->error('Sorry, that post has been deleted.');
            $this->redirect($this->referer());
        }
        
        // If repost_id has value, then it is reposted
        if (!empty($post->repost_id)) {
            // Get post_id of reposted post
            $repost = $this->Reposts->findById($post->repost_id)->contain(['Users'])->firstOrFail();
            // Get reposted post data using $repost->post_id
            $repost = $this->Posts->findById($repost->post_id)->contain(['Users'])->firstOrFail();
            $this->set('repost', $repost);
        }
        $this->set(compact('post'));
    }

    /**
     * Edit post
     * @param $id
     * @return object data
     */
    public function edit($id)
    {
        // Set layout
        $this->viewBuilder()->setLayout('logged_in_form');

        // Load Model
        $this->loadModel('Reposts');

        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        // Get post data
        $post =  $this->Posts->find('all',[
            'conditions' => ['Posts.id' => $id, 'user_id' => $user_id]
        ])->contain(['Users'])->first();
        if ($this->request->is(['post', 'put'])) {
            // Get data from form then save
            $this->Posts->patchEntity($post, $this->request->getData(), ['validate' => 'default']);
            if ($this->Posts->save($post)) {
                // Flash message
                $this->Flash->success(__('Your post has been updated.'));
                return $this->redirect(['action' => 'view', $id]);
            }
            // Flash message
            $this->Flash->error(__('Unable to update your post.'));
        }
        // If reposted
        if (!empty($post->repost_id)) {
            // Get post_id of reposted post
            $repost = $this->Reposts->findById($post->repost_id)->contain(['Users'])->firstOrFail();
            // Get reposted post data using $repost->post_id
            $repost = $this->Posts->findById($repost->post_id)->contain(['Users'])->firstOrFail();
            $this->set('repost', $repost);
        }
        
        if (empty($post)) {
            // Flash message
            $this->Flash->error(__('Post not found.'));
            return $this->redirect($this->referer());
        }

        if (!empty($post)) {
            $this->set('post', $post);
        }
    }

    /**
     * Delete post
     * @param $id
     * @return redirected page
     */
    public function delete($id)
    {
        // Load Model
        $this->loadModel('Reposts');

        // Component
        $this->loadComponent('DeleteRepost');

        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        $this->request->allowMethod(['post']);
        
        // Get post data
        $post = $this->Posts->find('all', [
            'conditions' => ['id' => $id, 'user_id' => $user_id]
        ])->first();

        if (empty($post)) {
            // Flash message
            $this->Flash->error(__('Post not found.'));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }

        // Set and save for - soft delete
        $post->deleted = date('Y-m-d H:i:s');
        $post->_is_deleted = 1;
        if ($this->Posts->save($post)) {
            /**
             * Count repost: If $post->repost_id is null it means it is original post
             * If $post->repost_id is not null then it it is reposted post
             */
            $repostCount = $this->Reposts->find('all', [
                'conditions' => ['id' => $post->repost_id, '_is_deleted' => 0]
            ])->count();
            
            // If one result, delete reposted data with quote and its reposted without quote
            if ($repostCount == 1) {
                $repostData = $this->Reposts->find('all', [
                    'conditions' => ['id' => $post->repost_id]
                ])->first();

                $repost = $this->Reposts->get($repostData->id);
                $repost->_is_deleted = 1;
                $repost->deleted = date('Y-m-d H:i:s');
                $this->Reposts->save($repost);

                // Delete all reposted without quote of this reposted post
                $this->DeleteRepost->deleteRepostWithoutQuote($id);
            }

            // If no result, delete all reposted data without quote
            if ($repostCount == 0) {
                $this->DeleteRepost->deleteRepostWithoutQuote($id);
            }

            // When it is deleted to the users view, redirect to users view
            if (
                $this->referer()
                == Router::url(['controller' => 'users', 'action' => 'view'], true)
            ) {
                // Flash message
                $this->Flash->success(__('You deleted a post.'));
                return $this->redirect(['controller' => 'users', 'action' => 'view']);
            }
            
            // Flash message
            $this->Flash->success(__('You deleted a post.'));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
    }
}