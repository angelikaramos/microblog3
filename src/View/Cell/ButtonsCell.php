<?php

namespace App\View\Cell;

use Cake\View\Cell;

class ButtonsCell extends Cell
{
    /**
     * Count likes
     * @param $post_id
     * @return int
     */
    public function countLike($post_id)
    {
        $this->loadModel('Likes');
        $likes = $this->Likes->find('all', [
            'conditions' => ['post_id' => $post_id, '_is_deleted' => 0]
        ]);
        $this->set('countLike', $likes->count());
    }

    /**
     * Count comments
     * @param $post_id
     * @return int
     */
    public function countComment($post_id)
    {
        $this->loadModel('Comments');
        $comments = $this->Comments->find('all', [
            'conditions' => ['post_id' => $post_id, '_is_deleted' => 0]
        ]);
        $this->set('countComment', $comments->count());
    }

    /**
     * Count reposts
     * @param $post_id
     * @return int
     */
    public function countRepost($post_id)
    {
        $this->loadModel('Reposts');
        $reposts = $this->Reposts->find('all', [
            'conditions' => ['post_id' => $post_id, '_is_deleted' => 0]
        ]);
        $this->set('countRepost', $reposts->count());
    }

    /**
     * Check if logged in user like certain post
     * @param $post_id, $user_id
     * @return int
     */
    public function checkLike($post_id, $user_id)
    {
        $this->loadModel('Likes');
        $liked = $this->Likes->find('all', [
            'conditions' => ['post_id' => $post_id, 'liker_user_id' => $user_id, '_is_deleted' => 0]
        ]);
        $this->set('liked', $liked->count());
        $this->set('post_id', $post_id);
    }

    /**
     * Check if logged in user repost certain post
     * @param $post_id, $user_id
     * @return int
     */
    public function checkRepostByPostId($post_id, $user_id)
    {
        $this->loadModel('Reposts');
        $this->loadModel('Posts');
        // Check if already reposted without quote, If reposted without quote: original post repost icon must be active
        $reposted = $this->Posts->find('all') 
            ->join([
                'reposts' => [
                    'table' => 'reposts',
                    'type' => 'INNER',
                    'conditions' => 'reposts.id = repost_id',
                ],
            ])
            ->where(['reposts.post_id' => $post_id, 'reposts.reposter_user_id' => $user_id, 'post_text' => ' ', 'reposts._is_deleted' => 0]);
        $reposts = $this->Reposts->find('all', [
            'conditions' => ['post_id' => $post_id, 'reposter_user_id' => $user_id, '_is_deleted' => 0]
        ]);

        // Get id of post of that reposted this post_id
        $post = $this->Posts->find('all') 
            ->join([
                'reposts' => [
                    'table' => 'reposts',
                    'type' => 'INNER',
                    'conditions' => 'reposts.id = repost_id',
                ],
            ])
        ->where(['reposts.post_id' => $post_id, 'reposts.reposter_user_id' => $user_id, 'post_text' => ' ', 'reposts._is_deleted' => 0])->first();

        // If reposted, post data is not empty
        if (!empty($post)) {
            $this->set('reposted_post_id', $post->id);
        }

        $this->set('reposted', $reposted->count());
        $this->set('reposts', $reposts->count());
        $this->set('post_id', $post_id);
        $this->set('user_id', $user_id);
    }
}