<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;

class RepostsController extends AppController
{
    /**
     * Executed before every action
     * 
     * @return redirected page
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
       
        // Load Model
        $this->loadModel('Users');
        
        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        // Get logged in user full_name for side_navbar
        if ($this->Auth->user()) {
            $loggedin_user = $this->Users->find('all', [
                'conditions' => ['id' => $user_id],
                'fields' => ['full_name'],
            ])->first();
        }
        $this->set('full_name', $loggedin_user->full_name);

        // Component
        $this->loadComponent('GetUser');

        // Get logged in user_id
        $user_id = $this->Auth->user('id');
        $this->set('loggedin_user_id', $user_id);

        // Get users not yet followed for side_navbar
        $usersNotfollowed = $this->GetUser->getUsersNotYetFollowed($user_id);
        $this->set('usersNotFollowed', $usersNotfollowed);
    }

    /**
     * Add repost
     * @param $post_id
     * @return redirected page
     */
    public function add($post_id)
    {
        // Set layout
        $this->viewBuilder()->setLayout('logged_in_form');

        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        // Load Model
        $this->loadModel('Posts');
        $this->LoadModel('Users');
        $this->loadModel('Reposts');

        // Get user data
        $user = $this->Users->findById($user_id)->firstorFail();
        // Get post data
        $post =  $this->Posts->findById($post_id)->contain(['Users'])->firstOrFail();
        // Get post data from form
        $postdata = $this->Posts->newEntity($this->request->getData(), ['validate' => 'default']);
        
        // Check validation
        if ($postdata->getErrors()) {
            // Flash Message
            $this->Flash->error(__('Post must be only 140 characters long.'));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }
        
        // If will repost again a quoted repost
        if (!empty($post->repost_id)) {
            // Get repost data using $post->repost_id
            $repost =  $this->Reposts->findById($post->repost_id)->contain(['Users'])->firstOrFail();
            // Get reposted post data using $repost->post_id
            $repost =  $this->Posts->findById($repost->post_id)->contain(['Users'])->firstOrFail();
            $this->set('repost', $repost);
        }
        if ($this->request->is(['post', 'put'])) {
            $repost = $this->Reposts->newEntity();
            // Set and save post_id and reposter_user_id in reposts table
            $repost->post_id = $post_id;
            $repost->reposter_user_id = $user_id;
            $repost->_is_deleted = 0;
            if ($this->Reposts->save($repost)) {
                // Set and save repost_id and user_id in posts table
                $postdata->repost_id = $repost->id;
                $postdata->user_id = $user_id;
                $postdata->_is_deleted = 0;
                $postdata->post_image = '';
                if ($this->Posts->save($postdata)) {
                    return $this->redirect(['controller' => 'posts', 'action' => 'index']);
                };
            }
        }
        $this->set('user', $user);
        $this->set('post', $post);
    }

    /**
     * Delete repost
     * @param $id
     * @return redirected page
     */
    public function delete($id)
    {
        $this->request->allowMethod(['post']);

        // Load Model
        $this->loadModel('Posts');

        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        // Get post data
        $post = $this->Posts->find('all', [
            'conditions' => ['id' => $id, 'user_id' => $user_id, '_is_deleted' => 0]
        ])->first();

        if (empty($post)) {
            // Flash message
            $this->Flash->error(__('Post not found.'));
            return $this->redirect($this->referer());
        }

        // Set and save for - soft delete for reposts table
        $post->deleted = date('Y-m-d H:i:s');
        $post->_is_deleted = 1;

        if ($this->Posts->save($post)) {
            $repost = $this->Reposts->find('all', [
                'conditions' => ['id' => $post->repost_id, 'reposter_user_id' => $user_id, '_is_deleted' => 0]
            ])->first();

            // Set and save for - soft delete for posts table
            $repost->deleted = date('Y-m-d H:i:s');
            $repost->_is_deleted = 1;
            if ($this->Reposts->save($repost)) {
                return $this->redirect(['controller' => 'posts', 'action' => 'index']);
            } 
        }
    }
}