<main>
    <div class="container-fluid">
        <!-- Post user -->
        <div class="post-user mt-3">
            <?= $this->Html->image('uploads/' . $post->user->profile_picture, [
                'alt' => 'Profile Picture', 'class' => 'thumb-img-post']
            ); ?>
            <?= $this->Html->link($post->user->full_name, ['controller' => 'users',
                'action' => 'view', $post->user->id], ['class' => 'text-decoration-none text-dark font-weight-bold']
            ); ?>
            <span><?= "@" . h($post->user->username); ?></span>
        </div>
        <div class="row">
            <!-- Edit Post -->  
            <div class="col-md-10 mt-3">
                <?= $this->Form->create($post, ['url' => ['action' => 'edit']]
                ); ?>
                <?= $this->Form->control('post_text', [
                        'cols' => '25',
                        'rows' => '5',
                        'id' => 'edit_post_text',
                        'class' => 'form-control mb-3',
                        'label' => false,
                ]); ?>
            </div>
            <div class="col-md-2 mt-5">
                <?= $this->Form->button(__('Save'), [
                    'type' => 'submit', 
                    'class' => 'form-control btn btn-primary',
                    'id' => 'edit_post'
                ]); ?>
                <?= $this->Form->end(); ?>
            </div>
        </div>
        <!-- Post image -->
        <?php if (!empty($post->post_image)):  ?>
            <a href="<?= $this->Url->build(['controller' => 'posts', 'action' => 'view', $post->id], ['escape' => false]); ?>" class="text-decoration-none text-dark">  
                <?= $this->Html->image('posts/' . h($post->post_image), [
                    'class' => 'thumb-post-image'
                ]); ?>
            </a>
        <?php endif; ?>
        <?php if (!empty($repost)): ?>
            <?php if ($repost->_is_deleted == 1): ?>
                <div class="card-header">
                    <p class="text-muted">This post is unavailable.</p>
                </div>
            <?php endif; ?>
            <?php if ($repost->_is_deleted == 0): ?>
                <div class="card mt-3">
                    <div class="card-header">
                        <?= $this->Html->image('uploads/' . $repost->user->profile_picture, [
                            'alt' => 'Profile Picture', 'class' => 'thumb-img-post']
                        ); ?>
                        <?= $this->Html->link($repost->user->full_name, ['controller' => 'users',
                        'action' => 'view', $repost->id], ['class' => 'text-decoration-none text-dark font-weight-bold']); ?>
                        <span><?= "@" . h($repost->user->username); ?></span>
                        <!-- Post date -->
                        <span class="card-text"><small class="text-muted"><?= "· " . date('M Y', strtotime($repost->modified)); ?></small></span>
                    </div>
                    <div class="card-body ml-5">
                        <!-- Repost post -->
                        <div class="repost ml-2">
                            <span class="post-content">
                                <!-- Post text -->
                                <?= h($repost->post_text); ?>
                            </span>
                            <!-- Post image -->
                            <?php if (!empty($repost->post_image)):  ?>
                                <a href="<?= $this->Url->build(['controller' => 'posts', 'action' => 'view', $repost->id], ['escape' => false]); ?>" class="text-decoration-none text-dark">  
                                    <?= $this->Html->image('posts/' . h($repost->post_image), [
                                        'class' => 'thumb-post-image mt-3'
                                    ]); ?>
                                </a>
                            <?php endif; ?>
                            <!-- Check if it is also reposted quote, if true get post_id then create url -->
                            <?= $this->cell('Post::getPostId', [$post->repost_id]) ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>   
        <?php endif; ?>
    </div>
</main>