<!DOCTYPE html>
<html lang="en">
	<head>
        <?= $this->Html->charset() ?>
        <title>
            <?php $this->assign('title', 'MICROBLOG'); ?>
            <?= $this->fetch('title') ?>
        </title>
        <?= $this->Html->meta('icon') ?>

        <!-- CSS files -->
        <?= $this->Html->css('bootstrap.min.css') ?>
        <?= $this->Html->css('all.min.css') ?>
        <?= $this->Html->css('styles.css') ?>
        
        <!-- JavaScript files -->
        <?= $this->Html->script('jquery.js') ?>
        <?= $this->Html->script('navigation.js') ?>
        <?= $this->Html->script('jquery.min.js', ['defer']); ?>
        <?= $this->Html->script('bootstrap.bundle.min.js', ['defer']) ?>
        <?= $this->Html->script('popper.min.js', ['defer']) ?>
        <?= $this->Html->script('script.js', ['defer']) ?>
        
        <?= $this->fetch('meta') ?>
        <?= $this->fetch('css') ?>
        <?= $this->fetch('script') ?>
	</head>
	<body>
		<div id="content">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
		</div>
	</body>
</html>