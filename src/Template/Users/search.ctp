<main>
    <div class="container-fluid">
        <h2 class="mt-4">Search Result  : <?= $keyword; ?></h2>
        <?php 
            if ($users->isEmpty()) {
                echo "<p class='text-center'> No results found.</p>";
            }
        ?>
        <!-- Search results -->
        <?php foreach ($users as $user): ?>
            <div class="following mt-4">
                <!-- User -->
                <?= $this->Html->image('uploads/' . $user->profile_picture, [
                    'alt' => 'Profile Picture', 'class' => 'thumb-img-post']
                ); ?>
                <strong>
                    <?= $this->Html->link($user->full_name, ['controller' => 'users','action' => 'view', $user->id], ['class' => 'text-decoration-none text-dark']) ?>
                </strong>
                <span><?= "@" . $user->username; ?></span>
                <!-- Follow or Following Button -->
                <?php 
                    $button = "follow";
                    foreach ($usersfollowing as $userfollowing) {
                        if ($userfollowing->user_id == $user->id) {
                            $button = "unfollow";
                        break;
                        } 
                        if ($userfollowing->user_id != $user->id) {
                            $button = "follow";
                        }
                    }
                ?>
                <!-- Button -->
                <span class="follow<?= $user->id ?>">
                    <?php 
                        if ($button == "follow") {
                            echo $this->Html->link('Follow', ['controller' => 'followers', 'action' => 'add', $user->id], ['data-id' => $user->id, 'class' => 'add-follow btn btn-primary btn-sm float-right mr-2 mb-2']);
                        }
                        if ($button == "unfollow") {
                            echo $this->Html->link('Unfollow', ['controller' => 'followers', 
                            'action' => 'delete',  $user->id], ['data-id' => $user->id, 'class' => 'delete-follow btn btn-primary active btn-sm float-right mr-2 mb-2']);
                        }
                    ?>
                </span>
            </div>
        <?php endforeach; ?>
    </div>
</main>
