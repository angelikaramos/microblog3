<div class="users form">
    <div class="login-container d-flex align-items-center justify-content-center">
        <?= $this->Form->create($user, [
            'div' => 'text-danger',
            'autocomplete' => 'off',
            'novalidate' => true
        ]); ?>
            <h4 class="mb-5 text-center">REGISTRATION</h4>
            <fieldset>
                <div class="error-message text-center">
                    <?= $this->Form->error('full_name', null, ['wrap' => 'label']); ?>
                </div>
                <?= $this->Form->control('full_name', [
                    'label' => false,
                    'class' => 'form-control rounded-pill form-control-lg mb-3',
                    'placeholder' => 'Full Name',
                    'required' => false,
                    'error' => false
                ]); ?>
                <div class="error-message text-center">
                    <?= $this->Form->error('email', null, ['wrap' => 'label']); ?>
                </div>
                <?= $this->Form->control('email', [
                    'label' => false,
                    'class' => 'form-control rounded-pill form-control-lg mb-3',
                    'placeholder' => 'Email',
                    'required' => false,
                    'error' => false
                ]); ?>
                <div class="error-message text-center">
                    <?= $this->Form->error('username', null, ['wrap' => 'label']); ?>
                </div>
                <?= $this->Form->control('username', [
                    'label' => false,
                    'class' => 'form-control rounded-pill form-control-lg mb-3',
                    'placeholder' => 'Username',
                    'required' => false,
                    'error' => false
                ]); ?>
                <div class="error-message text-center">
                    <?= $this->Form->error('password', null, ['wrap' => 'label']); ?>
                </div>
                <?= $this->Form->control('password', [
                    'label' => false,
                    'class' => 'form-control rounded-pill form-control-lg mb-3',
                    'placeholder' => 'Password',
                    'required' => false,
                    'error' => false
                ]); ?>
            </fieldset>
            <p class="mt-3 font-weight-normal">
                Already have an account?
                <?= $this->Html->link('Click here', [
                    'controller' => 'users',
                    'action' => 'login'
                ]); ?>
                to login.
            </p>
            <?= $this->Form->button(__('Register'), [
                'type' => 'submit', 
                'class' => 'btn btn-primary text-uppercase btn-block rounded-pill btn-lg'
            ]); ?>
        <?= $this->Form->end() ?>
    </div>
</div>