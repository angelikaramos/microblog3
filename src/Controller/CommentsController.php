<?php

namespace App\Controller;

use Cake\Event\Event;

class CommentsController extends AppController
{
    /**
     * Execeuted before every action
     * 
     * @return 
     */
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Component
        $this->loadComponent('GetUser');

        // Load Model
        $this->loadModel('Users');
        
        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        // Get logged in user full_name for side_navbar
        if ($this->Auth->user()) {
            $loggedin_user = $this->Users->find('all', [
                'conditions' => ['id' => $user_id],
                'fields' => ['full_name'],
            ])->first();
        }
        $this->set('full_name', $loggedin_user->full_name);

        // Get logged in user_id
        $user_id = $this->Auth->user('id');
        $this->set('loggedin_user_id', $user_id);

        // Get users not yet followed for side_navbar
        $usersNotfollowed = $this->GetUser->getUsersNotYetFollowed($user_id);
        $this->set('usersNotFollowed', $usersNotfollowed);
    }

    /**
     * Add comment
     * @param $post_id
     * @return redirected page
     */
    public function add($post_id)
    {
        // Get logged in user_id
        $commenter_user_id = $this->Auth->user('id');

        $comment = $this->Comments->newEntity();
        if ($this->request->is('post')) {
            // Get data from form
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());

            // Set post_id and commenter_user_id and save
            $comment->post_id = $post_id;
            $comment->commenter_user_id = $commenter_user_id;
            $comment->_is_deleted = 0;
            if ($this->Comments->save($comment)) {
                // Flash message
                $this->Flash->success(__('You added a comment'));
                return $this->redirect($this->referer());
            }
        }
    }

    /**
     * Edit comment
     * @param $id
     * @return redirected page
     */
    public function edit($id)
    {
        // Set layout
        $this->viewBuilder()->setLayout('logged_in_form');

        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        // Get comment by id
        $comment = $this->Comments->find('all', [
            'conditions' => ['Comments.id' => $id, 'commenter_user_id' => $user_id]
        ])->contain(['Users'])->first();

        if (empty($comment)) {
            // Flash message
            $this->Flash->error(__('Comment not found.'));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }

        if ($this->request->is(['post', 'put'])) {
            // Get data from form then save
            $this->Comments->patchEntity($comment, $this->request->getData());
            if ($this->Comments->save($comment)) {
                // Flash message
                $this->Flash->success(__('Your comment has been updated.'));
                return $this->redirect(['controller' => 'posts', 'action' => 'view', $comment->post_id]);
            }
        }
        $this->set('comment', $comment);
    }

    /**
     * Delete comment
     * @param $id
     * @return redirected page
     */
    public function delete($id)
    {
        $this->request->allowMethod(['post']);

        // Get logged in user_id
        $user_id = $this->Auth->user('id');

        // Get comment by id
        $comment = $this->Comments->find('all', [
            'conditions' => ['Comments.id' => $id, 'commenter_user_id' => $user_id
        ]])->contain(['Users'])->first();

        if (empty($comment)) {
            // Flash message
            $this->Flash->error(__('Comment not found.'));
            return $this->redirect(['controller' => 'posts', 'action' => 'index']);
        }

        // Set and save for - soft delete
        $comment->deleted = date('Y-m-d H:i:s');
        $comment->_is_deleted = 1;

        if ($this->Comments->save($comment)) {
            // Flash message
            $this->Flash->success(__('Your comment has been deleted.'));
            $this->redirect($this->referer());
        }
    }
}