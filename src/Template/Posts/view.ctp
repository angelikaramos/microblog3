<main>
    <div class="container-fluid">
        <?php
            if ($post->repost_id != '') {
                // Repost
                if ($post->post_text == '') {
                    echo $this->element('view_repost', [
                    'repost_post' => true,
                    'post_user_id' => $post->user_id,
                    'post_id' => $post->id,
                    'post_post_text' => $post->post_text,
                    'post_username' => $post->user->username,
                    'repost_repost_id' => $repost->repost_id,
                    'repost_post_username' => $repost->user->username,
                    'repost_user_id' => $repost->user->id,
                    'repost_id' => $repost->id,
                    'repost_user_profile_picture' => $repost->user->profile_picture,
                    'repost_user_full_name' => $repost->user->full_name,
                    'repost_user_username' => $repost->user->username,
                    'repost_post_text' => $repost->post_text,
                    'repost_post_image' => $repost->post_image,
                    'repost_modified' => $repost->modified
                    ]);
                }
                // Repost with Quote
                if ($post->post_text != '') {
                    if (!empty($repost)) {
                        echo $this->element('view_repost_with_quote', [
                        'post_user_id' => $post->user_id,
                        'post_id' => $post->id,
                        'post_post_text' => $post->post_text,
                        'post_user_profile_picture' => $post->user->profile_picture,
                        'post_user_full_name' => $post->user->full_name,
                        'post_user_username' => $post->user->username,
                        'post_repost_id' => $post->repost_id,
                        'repost_post_username' => $repost->user->username,
                        'repost_user_id' => $repost->user_id,
                        'repost_id' => $repost->id,
                        'repost_user_profile_picture' => $repost->user->profile_picture,
                        'repost_user_full_name' => $repost->user->full_name,
                        'repost_user_username' => $repost->user->username,
                        'repost_post_text' => $repost->post_text,
                        'repost_post_image' => $repost->post_image,
                        'repost_modified' => $repost->modified,
                        'repost_is_deleted' => $repost->_is_deleted
                        ]);
                    }
                }
            }
        ?>
        <?php 
            if ($post->repost_id == '') {
                // Original Post
                if (empty($repost)) {
                    echo $this->element('view_original_post', [
                    'repost_post' => false,
                    'post_user_id' => $post->user_id,
                    'post_id' => $post->id,
                    'post_user_profile_picture' => $post->user->profile_picture,
                    'post_user_full_name' => $post->user->full_name,
                    'post_user_username' => $post->user->username,
                    'post_post_text' => $post->post_text,
                    'post_post_image' =>$post->post_image,
                    'post_modified' => $post->modified
                    ]);
                } 
            }
        ?>
        <!-- Add Comment -->
        <div class="row">
            <div class="col-md-10">
                <?php
                    // Comment in reposted without quote or reposted with quote
                    if ($post->repost_id != '') {
                        if ($post->post_text == null) {
                            $url = ['controller' => 'comments', 'action' => 'add', $repost->id];
                            echo $this->Form->create(null, [
                                'url' => $url
                            ]);
                        }
                        if ($post->post_text != null) {
                            $url = ['controller' => 'comments', 'action' => 'add', $post->id];
                            echo $this->Form->create(null, [
                                'url' => $url
                            ]);
                        }
                    }
                    // Comment in original post
                    if ($post->repost_id == '') {
                        $url = ['controller' => 'comments', 'action' => 'add', $post->id];
                        echo $this->Form->create(null, [
                            'url' => $url
                        ]);
                    }
                ?>
                <?= $this->Form->control('comment_text', [
                    'cols' => '20',
                    'rows' => '3',
                    'id' => 'comment_text',
                    'class' => 'form-control',
                    'placeholder' => 'Comment your thoughts here..',
                    'required' => false,
                    'label' => false
                ]); ?>
            </div>
            <div class="col-md-2 mt-3">
                <?= $this->Form->button(__('Comment'), [
                    'class' => 'form-control btn btn-primary',
                    'id' => 'comment'
                ]); ?>
                <?= $this->Form->end(); ?>
            </div>
        </div>
        <!-- Comments -->
        <div class="row mt-3">
            <div class="col-md-12">
                <?php
                    // Repost
                    if ($post->repost_id != '') {
                        // Without Quote
                        if ($post->post_text == null) {
                            echo $this->cell('Comment::getComments', [$repost->id, $loggedin_user_id]);
                        }
                        // With Quote
                        if ($post->post_text != null) {
                            echo $this->cell('Comment::getComments', [$post->id, $loggedin_user_id]);
                        }
                    }
                ?>
                <?php 
                    // Original Post
                    if ($post->repost_id == '') {
                        echo $this->cell('Comment::getComments', [$post->id, $loggedin_user_id]);
                    }
                ?>
            </div>
        </div>
    </div>
</main>
        
