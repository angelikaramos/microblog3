$(document).ready(function(){
    // Search User
    $('#search').attr('disabled',true);
    $('#keyword').keyup(function(){
        if ($(this).val().trim() === '') {
            $('#search').attr('disabled',true);     
        } else if ($(this).val().length != 0) {
            $('#search').attr('disabled',false);     
        }  else {
            $('#search').attr('disabled',true);
        }
    });

    // Create Post
    $('#post').attr('disabled',true);
    $('#post_image').on('keyup change', function() {
        if ($('#post_image').val() != '') {
            $('#post').attr('disabled',false);
        }
    });
    $('#post_text').keyup(function(){
        if ($(this).val().trim() === '' && $('#post_image').val() == '') {
            $('#post').attr('disabled',true);
        } else if ($(this).val().length != 0 || $('#post_image').val() != '') {
            $('#post').attr('disabled',false);     
        } else {
            $('#post').attr('disabled',true);
        }
    }); 

    // Thumbnail of image to be post
    $('#thumb_post_image').hide();
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            
            reader.onload = function(e) {
                $('#thumb_post_image').attr('src', e.target.result);
                $('#thumb_post_image').show();
            }
            reader.readAsDataURL(input.files[0]); // convert to base64 string
        }
    }
      
    $("#post_image").change(function() {
        readURL(this);
    });
   
    // Edit Post
    $('#edit_post').attr('disabled',true);
    $('#edit_post_text').keyup(function(){
        if ($(this).val().trim() === '') {
            $('#edit_post').attr('disabled',true);     
        } else if ($(this).val().length != 0) {
            $('#edit_post').attr('disabled',false);     
        }  else {
            $('#edit_post').attr('disabled',true);
        }
    });

    // Comment to a Post
    $('#comment').attr('disabled',true);
    $('#comment_text').keyup(function(){
        if ($(this).val().trim() === '') {
            $('#comment').attr('disabled',true);
        } else if ($(this).val().length != 0) {
            $('#comment').attr('disabled',false);     
        } else {
            $('#comment').attr('disabled',true);
        }
    });

    // Edit a comment to a Post
    $('#edit_comment').attr('disabled',true);
    $('#edit_comment_text').keyup(function(){
        if ($(this).val().trim() === '') {
            $('#edit_comment').attr('disabled',true);
        } else if ($(this).val().length != 0) {
            $('#edit_comment').attr('disabled',false);     
        } else {
            $('#edit_comment').attr('disabled',true);
        }
    });

    // Create Repost
    $('#repost').attr('disabled',false)
    $('#repost_text').keyup(function(){
        if ($(this).val().length === 0) {
            $('#repost').attr('disabled',false);     
        } else if ($(this).val().trim() === '') {
            $('#repost').attr('disabled',true);     
        } else {
            $('#repost').attr('disabled',false);
        }
    });
});