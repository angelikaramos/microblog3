$(document).ready(function () {
    $(".add-like").unbind().bind('click', function(event){
        event.preventDefault();
        // Get post_id 
        var post_id = this.dataset.post;
        var btnLike = "#btnLike"+post_id;
        // Prevent user to double send request
        var alreadyClicked = $(this).data('clicked');
        if (alreadyClicked) {
            return false;
        }
        $(this).data('clicked', true);
        $.ajax({
            async:true, 
            dataType:"html", 
            success:function (data, textStatus) {
                $(btnLike).html(data);
                $(this).data('clicked', false);
            }, 
            url:$(this).attr('href')
        });
        return false;
    });

    $(".delete-like").unbind().bind('click', function(event){
        event.preventDefault();
        // Get post_id 
        var post_id = this.dataset.post;
        var btnLike = "#btnLike"+post_id;
        // Prevent user to double send request
        var alreadyClicked = $(this).data('clicked');
        if (alreadyClicked) {
            return false;
        }
        $(this).data('clicked', true);
        $.ajax({
            async:true, 
            dataType:"html", 
            success:function (data, textStatus) {
                $(btnLike).html(data);
                $(this).data('clicked', false);
            }, 
            url:$(this).attr('href')
        });
        return false;
    });
});