<div class="post mt-3">
    <!-- Add dropdown_post element if logged in user is the one who post -->
    <?php 
        if ($loggedin_user_id == $post_user_id) {
            echo $this->element('dropdown_post', ['post_id' => $post_id]); 
        }
    ?>
    <!-- Post user -->
    <div class="post-user mt-2">
        <?= $this->Html->image('uploads/' . $post_user_profile_picture, ['alt' => 'Profile Picture', 'class' => 'thumb-img-post']); ?>
        <?= $this->Html->link($post_user_full_name, ['controller' => 'users',
        'action' => 'view', $post_user_id], ['class' => 'text-decoration-none text-dark font-weight-bold']) ?>
        <span><?= "@" . h($post_user_username); ?></span>
    </div>
    <!-- Post -->
    <span class="post-content custom-select-lg ml-5 mb-2"><?= h($post_post_text); ?></span>
    <?php if ($repost_is_deleted == 1): ?>
        <?= $this->element('url_post', ['repost_post_id' => $repost_id]); ?>
    <?php endif; ?>
    <!-- Reposted post -->
    <div class="card mt-2 ml-5">
        <?php if ($repost_is_deleted == 1): ?>
            <div class="card-header">
                <p class="text-muted">This post is unavailable.</p>
            </div>
        <?php endif; ?>
        <?php if ($repost_is_deleted == 0): ?>
            <div class="card-header">
                <!-- Reposted Post user -->
                <?= $this->Html->image('uploads/' . $repost_user_profile_picture, ['alt' => 'Profile Picture', 'class' => 'thumb-img-post']); ?>
                <?= $this->Html->link($repost_user_full_name, ['controller' => 'users',
                'action' => 'view', $repost_user_id], ['class' => 'text-decoration-none text-dark font-weight-bold']); ?>
                <span><?= "@" . h($repost_user_username); ?></span>
                <!-- Post date -->
                <span class="card-text">
                    <small class="text-muted">
                        <?= "· " . date('M Y', strtotime($repost_modified)); ?>
                    </small>
                </span>
            </div>
            <a href="<?= $this->Url->build(['controller' => 'posts', 'action' => 'view', $repost_id], ['escape' => false]); ?>" class="text-decoration-none text-dark">
                <div class="card-body">
                    <!-- Reposted Post -->
                    <div class="post ml-5">
                        <?php if (empty($repost_post_image)): ?>
                            <!-- Reposted post text -->
                            <span class="post-content"><?= h($repost_post_text); ?></span>
                        <?php endif; ?>
                        <?php if (!empty($repost_post_image)): ?>
                            <!-- Reposted post text -->
                            <p class="post-content"><?= h($repost_post_text); ?></p>
                            <!-- Reposted post image -->
                            <?= $this->Html->image('posts/' . h($repost_post_image), [
                                'class' => 'rounded thumb-post-image'
                            ]); ?>    
                        <?php endif; ?>
                        <!-- Check if it is also reposted quote, if true get post_id then create url -->
                        <?= $this->cell('Post::getPostId', [$post_repost_id]) ?>
                    </div>
                </div>
            </a>
        <?php endif; ?>
    </div>
    <!-- Reposted post date -->
    <p class="text-muted small mt-2"><?= date('g:i A · M, j Y', strtotime($repost_modified)); ?></p>
    <hr>
    <!-- Buttons -->
    <?= $this->element('view_buttons', ['post_id' => $post_id]) ?>
</div>
<hr>